const client = require('superagent')
const config = require('../config')

const lmsBaseUrl = config.lmsEndpoints.baseUrl
const lmsPrefix = config.lmsEndpoints.prefix
const token = config.lmsEndpoints.token
const tokenParamName = config.lmsEndpoints.tokenParamName
const formatParamName = config.lmsEndpoints.formatParamName
const functionParamName = config.lmsEndpoints.functionParamName
const restFormat = config.lmsEndpoints.restFormat

async function makeLmsRequest(method, bodyParams) {
  if (!method) {
    throw new Error("Missing argument: method")
  }

  let b = new URL(lmsBaseUrl)
  b.searchParams.append(tokenParamName, token)
  b.searchParams.append(formatParamName, restFormat)
  b.searchParams.append(functionParamName, lmsPrefix + method)

  let normalizedBodyParams = normalizeBodyParams(bodyParams)
  let form = buildBodyForm(normalizedBodyParams)

  const result = await client.post(b)
    .set("Accept", "application/json")
    .set("Content-Type", "application/x-www-form-urlencoded")
    .type('form')
    .send(form)

  return result
}

function normalizeBodyParams(bodyParams) {
  let normalizedMap = {}

  if (!bodyParams) {
    return normalizedMap
  }
  for (const [key, value] of Object.entries(bodyParams)) {
    let stringKey = key
    if (hasBracketNotation(stringKey)) {
      if (hasValueInBrackets(stringKey)) {
        normalizedMap = complexKeyToMapStructure(normalizedMap, stringKey, value)
      } else {
        normalizedMap = emptyArrayKeyToMapStructure(normalizedMap, stringKey, value)
      }
    } else {
      normalizedMap[stringKey] = collectionsToMapStructure(value)
    }
  }

  return normalizedMap
}

function hasBracketNotation(param) {
  return (param.includes("[") && param.includes("]"))
}

function hasValueInBrackets(param) {
  let firstIndex = param.indexOf('[')
  let secondIndex = param.indexOf(']')
  return (secondIndex > (firstIndex + 1))
}

function complexKeyToMapStructure(body, key, value) {
  let stringKey = key
  let stringArray = stringKey.split("\\\[")
  let result = body
  for (let i = 0; i < stringArray.length; i++) {
    let splitKey = stringArray[i].replace(']', '')
    if (i < stringArray.length - 1) {
      if (!result.contains(splitKey)) {
        result[splitKey] = {}
      }
      result = result[splitKey]
    } else {
      result[splitKey] = value.toString()
    }
  }

  return body
}

function emptyArrayKeyToMapStructure(body, key, value) {
  let arrayKey = key.replace('[','').replace(']','')
  body[arrayKey] = {}
  if (Array.isArray(value)) {
    for (const [i, arrayValue] of value.entries()) {
      body[arrayKey][i.toString()] = arrayValue.toString()
    }
  } else {
    body[arrayKey]["0"] = value.toString()
  }
}

function collectionsToMapStructure(value) {
  let body = {}

  if (Array.isArray(value)) {
    let size = value.length
    for (let i = 0; i < size; i++) {
      body[i.toString()] = collectionsToMapStructure(value[i])
    }
  } else if (value instanceof Map) {
    for (const [key, it] of Object.entries(value)) {
      body[key] = collectionsToMapStructure(it)
    }
  } else {
    return value.toString()
  }

  return body
}

function buildBodyForm(bodyParams) {
  let form = {}

  if (!bodyParams) {
    return form
  }
  for (const [key, value] of Object.entries(bodyParams)) {
    let arrayKey = key
    mapStructureToComplexKeyMap(form, value, arrayKey)
  }

  return form
}

function mapStructureToComplexKeyMap(body, value, baseKey) {
  mapStructureToComplexKeyMap(body, value, baseKey, "")
}

function mapStructureToComplexKeyMap(body, value, baseKey, builtMapKeys) {
  let key = builtMapKeys ? builtMapKeys : ""

  if (value instanceof Map) {
    for (const [k, v] of Object.entries(value)) {
      let stringKey = key + "[" + k + "]"
      body = mapStructureToComplexKeyMap(body, v, baseKey, stringKey)
    }
  } else {
    body["param[" + baseKey + "]" + key] = value
  }
}

module.exports = { makeLmsRequest }
