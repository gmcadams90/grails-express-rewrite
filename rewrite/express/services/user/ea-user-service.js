const { models } = require('../../../sequelize');

async function getCurrentUser(header) {
  let token = header.split(/\s+/).pop() || ''; // encoded part
  let userToken = await models.oauth_user_tokens.findOne({where: {token: token}})
  return await models.ea_user.findOne({where: {id: userToken.eaUserId}})
}

async function setLastAccess(user) {
  user.lastAccess = new Date()
  user.save()
}

module.exports = { getCurrentUser, setLastAccess }
