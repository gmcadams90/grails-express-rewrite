const express = require('express');
const bodyParser = require('body-parser');
const auth = require('./authentication');
const routeRoles = require('./config/route-roles.json');

const routes = {
	"permissions": require('./routes/user/permission'),
	"permission-roles": require('./routes/user/permission-role'),
	"training-locations": require('./routes/program/training-location'),
	"ea-users": require('./routes/user/ea-user'),
  "subscription-training-location-tiers": require('./routes/subscription/subscription-training-location-tier'),
  "cart-items": require('./routes/order/cart-item'),
  "courses": require('./routes/course/course'),
  "course-groups": require('./routes/course/course-group'),
  "course-sessions": require('./routes/course/course-session'),
  "custom-courses": require('./routes/course/custom-course'),
  "prerequisites": require('./routes/course/prerequisite'),
  "assigned-ebooks": require('./routes/ebook/assigned-ebook'),
  "ebook-transactions": require('./routes/ebook/ebook-transaction'),
  "ebook-transaction-groups": require('./routes/ebook/ebook-transaction-group'),
  "user-types": require('./routes/user/user-type'),
  "import-errors": require('./routes/program/import-error'),
  "readynamic-create-account-access-tokens": require('./routes/ebook/readynamic-create-account-access-token'),
  "completion-transactions": require('./routes/order/completion-transaction'),
  "access-logs": require('./routes/access-log'),
  "program-classifications": require('./routes/user/program-classification'),
  "pending-enrollments": require('./routes/order/pending-enrollment'),
  "submitted-enrollments": require('./routes/order/submitted-enrollment'),
  "submitted-orders": require('./routes/order/submitted-order'),
  "submitted-order-items": require('./routes/order/submitted-order-item'),
  "qualification-types": require('./routes/qualification/qualification-type'),
  "enrollment-transactions": require('./routes/subscription/enrollment-transaction'),
  "moodle-users": require('./routes/user/moodle-user'),
  "subscription-features": require('./routes/subscription/subscription-feature'),
  "enrollment-transaction-course-users": require('./routes/subscription/enrollment-transaction-course-user'),
  "enrollment-transaction-courses": require('./routes/subscription/enrollment-transaction-course'),
  "training-location-groups": require('./routes/program/training-location-group'),
  "access-requests": require('./routes/user/access-request'),
  "filter-groups": require('./routes/user/filter-group'),
  "subscriptions": require('./routes/subscription/subscription'),
  "subscription-prices": require('./routes/subscription/subscription-price'),
  "subscription-users": require('./routes/subscription/subscription-user'),
  "course-completions": require('./routes/order/course-completion'),
  "saved-searches": require('./routes/program/saved-search'),
  "import-runs": require('./routes/program/import-run'),
  "student-types": require('./routes/user/student-type'),
  "configurations": require('./routes/program/configuration'),
  "access-tokens": require('./routes/accesstoken/access-token'),
  "access-token-users": require('./routes/accesstoken/access-token-user'),
  "access-windows": require('./routes/accesstoken/access-window'),
  "training-location-access-windows": require('./routes/accesstoken/training-location-access-window'),
  "assigned-ica-course-groups": require('./routes/instructorcurriculumaccess/assigned-ica-course-group'),
  "assigned-ica-licenses": require('./routes/instructorcurriculumaccess/assigned-ica-license'),
  "ica-course-groups": require('./routes/instructorcurriculumaccess/ica-course-group'),
  "ica-licenses": require('./routes/instructorcurriculumaccess/ica-license'),
  "assigned-licenses": require('./routes/subscription/assigned-license'),
  "access-transactions": require('./routes/subscription/access-transaction')
};

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// We create a wrapper to workaround async errors not being transmitted correctly.
function makeHandlerAwareOfAsyncErrors(handler) {
	return async function(req, res, next) {
	  try {
			await handler(req, res);
		} catch (error) {
			next(error);
		}
	};
}

function authenticate(routeName, method) {
  return async function(req, res, next) {
    try {
      let permissions = routeRoles[routeName]
      let authCheck
      // Any role
      if (!permissions) {
        authCheck = await auth.verifyAccess(req, res, true)
      } else {
        permissions = permissions[method]
        if (permissions == "ALL") { // Permit all
          authCheck = await auth.verifyAccess(req, res, false)
        } else if (permissions == "ANY") { // Any role
          authCheck = await auth.verifyAccess(req, res, true)
        } else { // Specific roles
          authCheck = await auth.verifyAccess(req, res, true, permissions)
        }
      }
      // Only continue if there's no error
      if (!("error" in authCheck)) {
        next()
      } else {
        // Else, respond with error
        res.status(400).json(authCheck)
      }
    } catch (error) {
      next(error);
    }
  };
}

// We define the standard REST APIs for each route (if they exist).
for (const [routeName, routeController] of Object.entries(routes)) {
	if (routeController.getAll) {
		app.get(
			`/api/${routeName}`,
      authenticate(routeName, 'GET'),
			makeHandlerAwareOfAsyncErrors(routeController.getAll)
		);
	}
	if (routeController.getById) {
		app.get(
			`/api/${routeName}/:id`,
      authenticate(routeName, 'GET'),
			makeHandlerAwareOfAsyncErrors(routeController.getById)
		);
	}
	if (routeController.create) {
		app.post(
			`/api/${routeName}`,
      authenticate(routeName, 'POST'),
			makeHandlerAwareOfAsyncErrors(routeController.create)
		);
	}
	if (routeController.update) {
		app.put(
			`/api/${routeName}/:id`,
      authenticate(routeName, 'PUT'),
			makeHandlerAwareOfAsyncErrors(routeController.update)
		);
	}
	if (routeController.remove) {
		app.delete(
			`/api/${routeName}/:id`,
      authenticate(routeName, 'DELETE'),
			makeHandlerAwareOfAsyncErrors(routeController.remove)
		);
	}
}

// LMS pass through
app.post(
  `/lmsapi/:method`,
  authenticate('lmsapi', 'POST'),
  makeHandlerAwareOfAsyncErrors(require('./routes/lms-pass-through').index)
);

// Auth routes
app.post('/api/oauth/token', makeHandlerAwareOfAsyncErrors(require('./authentication').getAccessToken))

module.exports = app;
