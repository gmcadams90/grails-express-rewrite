const { models } = require('../../../sequelize');
const { serialize, serializeSingleton, getIdParam, createAndUpdate, findOne } = require('../../helpers');

const model = models.course_session
const relModels = [models.training_location, models.course, models.course_group, models.cart_item,
  {model: models.course_session, as: 'parent_session'}]

async function getAll(req, res) {
  let instances = null
  let where = {"deleted": false}

  if (req.query.trainingLocationId) {
    where['trainingLocationId'] = req.query.trainingLocationId
  }
  if (req.query.courseId) {
    where['courseId'] = req.query.courseId
  }
  if (req.query.courseGroupId) {
    where['courseGroupId'] = req.query.courseGroupId
  }
  if (req.query.pending) {
    where["pending"] = String(req.query.pending).toLowerCase() == "true"
  }
  if (req.query.transitory) {
    where["transitory"] = String(req.query.transitory).toLowerCase() == "true"
  }
  if (req.query.lmsId) {
    where["lmsId"] = req.query.lmsId
  }
  if (req.query.ids) {
    let idList = req.query.ids.split(',')
    instances = await model.findAll({
      where: {
        "id": idList
      },
      include: relModels
    })
  } else {
    instances = await model.findAll({
      where: where,
      include: relModels
    });
  }

  res.status(200).json(serialize(instances, model));
}

async function getById(req, res) {
  const id = getIdParam(req);
  const instance = await findOne(id, model, relModels)
  if (instance) {
    res.status(200).json(serializeSingleton(instance, model, id));
  } else {
    res.status(404).send('404 - Not found');
  }
}

async function create(req, res) {
  if (req.params.id) {
    res.status(400).send(`Bad request: ID should not be provided, since it is determined automatically by the database.`)
  } else {
    let instance = await createAndUpdate(req, model, relModels)
    res.status(201).json(serializeSingleton(instance, model, instance["dataValues"]["id"]))
  }
}

async function update(req, res) {
  let instance = await createAndUpdate(req, model, relModels)
  if (instance) {
    res.status(201).json(serializeSingleton(instance, model, instance["dataValues"]["id"]))
  } else {
    res.status(404).send('404 - Not found');
  }
}

async function remove(req, res) {
  const id = getIdParam(req);
  //TODO: Move delete into this
  //courseSessionService.deleteCourseSession(id)
  let instance = await findOne(id, model, relModels)
  if (instance.cartItem != null) {
    throw new Error("Course Session has a Cart Item and cannot be deleted." + instance.toString())
  }
  if (instance.lmsId > 0) {
    throw new Error("Course Session exists in the LMS and cannot be deleted." + instance.toString())
  }
  instance.deleted = true
  await instance.save()
  /*let cis = await cs.getCart_items()
  cis.forEach(ci => cs.removeCart_item(ci))
  await cs.destroy();*/
  res.status(200).end();
}

module.exports = {
  getAll,
  getById,
  create,
  update,
  remove
};
