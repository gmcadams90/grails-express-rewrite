const { models } = require('../../../sequelize');
const { serialize, serializeSingleton, getIdParam, createAndUpdate, findOne } = require('../../helpers');

const model = models.course_group
const relModels = [models.training_location, models.course, models.course_session]

async function getAll(req, res) {
  let where = {"deleted": false}

  if (req.query.trainingLocationId) {
    where['trainingLocationId'] = req.query.trainingLocationId
  }

  const instances = await model.findAll({
    where: where,
    include: relModels
  });
  res.status(200).json(serialize(instances, model));
}

async function getById(req, res) {
  const id = getIdParam(req);
  const instance = await findOne(id, model, relModels)
  if (instance) {
    res.status(200).json(serializeSingleton(instance, model, id));
  } else {
    res.status(404).send('404 - Not found');
  }
}

async function create(req, res) {
  if (req.params.id) {
    res.status(400).send(`Bad request: ID should not be provided, since it is determined automatically by the database.`)
  } else {
    let instance = await createAndUpdate(req, model, relModels)
    res.status(201).json(serializeSingleton(instance, model, instance["dataValues"]["id"]))
  }
}

async function update(req, res) {
  let instance = await createAndUpdate(req, model, relModels)
  if (instance) {
    res.status(201).json(serializeSingleton(instance, model, instance["dataValues"]["id"]))
  } else {
    res.status(404).send('404 - Not found');
  }
}

async function remove(req, res) {
  const id = getIdParam(req);
  let instance = await findOne(id, model, relModels)
  /*let rels = await instance.getCourse_sessions()
  if (rels) {
    rels.forEach( courseSession => {
      if (courseSession.courseGroupSession) {
        courseSession.courseGroupSession = false
        courseSession.isChildGroupSession = false
        courseSession.courseGroup = null
        courseSession.parentSession = null
        courseSession.save()
      }
      courseSession.courseSessions = []
      courseSession.deleted = true
      instance.removeCourse_session(courseSession)
    })
  }*/

  instance.deleted = true;
  await instance.save();
  res.status(200).end();
}

module.exports = {
  getAll,
  getById,
  create,
  update,
  remove
};
