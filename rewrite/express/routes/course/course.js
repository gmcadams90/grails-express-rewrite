const { models } = require('../../../sequelize');
const { serialize, serializeSingleton, getIdParam, createAndUpdate, findOne } = require('../../helpers');

const model = models.course
const relModels = [models.training_location, models.course_group, models.course_session]

async function getAll(req, res) {
  let instances = null

  if (req.query.ids) {
    let idList = req.query.ids.split(',')
    instances = await model.findAll({
      where: {
        "id": idList,
        "deleted": false
      },
      include: relModels
    })
  } else {
    instances = await model.findAll({
      where: {
        "deleted": false
      },
      include: relModels
    });
  }
  res.status(200).json(serialize(instances, model));
}

async function getById(req, res) {
  const id = getIdParam(req);
  const instance = await findOne(id, model, relModels)
  if (instance) {
    res.status(200).json(serializeSingleton(instance, model, id));
  } else {
    res.status(404).send('404 - Not found');
  }
}

async function create(req, res) {
  if (req.params.id) {
    res.status(400).send(`Bad request: ID should not be provided, since it is determined automatically by the database.`)
  } else {
    let instance = await createAndUpdate(req, model, relModels)
    res.status(201).json(serializeSingleton(instance, model, instance["dataValues"]["id"]))
  }
}

async function update(req, res) {
  let instance = await createAndUpdate(req, model, relModels)
  if (instance) {
    res.status(201).json(serializeSingleton(instance, model, instance["dataValues"]["id"]))
  } else {
    res.status(404).send('404 - Not found');
  }
}

async function remove(req, res) {
  const id = getIdParam(req);

  let instance = await findOne(id, model, relModels)
  instance.deleted = true;
  await instance.save();
 /*let rels = await instance.getTraining_locations()
  rels.forEach(rel => instance.removeTraining_location(rel));
  rels = await instance.getCourse_groups()
  rels.forEach(rel => instance.removeCourse_group(rel));
  rels = await instance.getCourse_sessions()
  rels.forEach(rel => instance.removeCourse_session(rel));
  await instance.destroy();*/
  res.status(200).end();
};

module.exports = {
  getAll,
  getById,
  create,
  update,
  remove
};
