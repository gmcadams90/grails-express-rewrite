const { makeLmsRequest } = require('../services/lms-request-service')
const config = require('../config')
const { handleMissingParameter } = require("../helpers");

const instructorCourseId = config.autoEnrollConfiguration.instructorCourse.lmsCourseId
const demoCourseId = config.autoEnrollConfiguration.demoCourse.lmsCourseId
const instructorCourseAccessRequestUserType = config.autoEnrollConfiguration.instructorCourse.accessRequestUserType
const instructorCourseDefaultUserType = config.autoEnrollConfiguration.instructorCourse.defaultUserType
const demoCourseAccessRequestUserType = config.autoEnrollConfiguration.demoCourse.accessRequestUserType
const demoCourseDefaultUserType = config.autoEnrollConfiguration.demoCourse.defaultUserType

async function index(req, res) {
  let method = req.params.lms_method ? req.params.lms_method : req.params.method
  if (!method) {
    handleMissingParameter("method")
  }

  await handleLmsRequest(res, method, req.body)
}

async function handleLmsRequest(res, method, reqparams) {
  let resp = await makeLmsRequest(method, reqparams)
  let status = resp.status

  if (!resp.success) {
    status = 500
  }

  res.status(status).json(resp.body)
}

module.exports = {
  index
}
