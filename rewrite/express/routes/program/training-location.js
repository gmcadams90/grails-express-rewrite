const { models } = require('../../../sequelize');
const { serialize, serializeSingleton, getIdParam, createAndUpdate, findOne } = require('../../helpers');

const model = models.training_location
const relModels = [models.permission_role, {model: models.ea_user}, {model: models.subscription_training_location_tier}, {model: models.cart_item}]

async function getAll(req, res) {
  let where = {}

  if (!req.query.showAll) {
    where['subscription_enabled'] = true
  }
  if (req.query.excludeHidden) {
    relModels[1]["where"] = {"deleted": false}
    where['hidden'] = false
  }
  if (req.query.subscriptionTrainingLocationTierId) {
    relModels[2]["where"] = {"id": req.query.subscriptionTrainingLocationTierId}
  }
  if (!req.query.withDeleted) {
    where['deleted'] = false
  }

  const instances = await model.findAll({
    where: where,
    include: relModels
  });
  res.status(200).json(serialize(instances, model));
}

async function getById(req, res) {
  const id = getIdParam(req);
  relModels[3]["where"] = {"deleted": false}
  const instance = await findOne(id, model, relModels)
  if (instance) {
    res.status(200).json(serializeSingleton(instance, model, id));
  } else {
    res.status(404).send('404 - Not found');
  }
}

async function create(req, res) {
  if (req.params.id) {
    res.status(400).send(`Bad request: ID should not be provided, since it is determined automatically by the database.`)
  } else {
    let instance = await createAndUpdate(req, model, relModels)
    res.status(201).json(serializeSingleton(instance, model, instance["dataValues"]["id"]))
  }
}

async function update(req, res) {
  let instance = await createAndUpdate(req, model, relModels)
  if (instance) {
    res.status(201).json(serializeSingleton(instance, model, instance["dataValues"]["id"]))
  } else {
    res.status(404).send('404 - Not found');
  }
}

async function remove(req, res) {
  const id = getIdParam(req);

  let instance = await findOne(id, model, relModels)
  instance.deleted = true;
  await instance.save();
  /*let rels = await instance.getPermissions()
  rels.forEach(rel => instance.removePermission(rel));
  rels = await instance.getEa_users()
  rels.forEach(rel => instance.removeEa_user(rel));
  await instance.destroy();*/
  res.status(200).end();
};

module.exports = {
  getAll,
  getById,
  create,
  update,
  remove
};
