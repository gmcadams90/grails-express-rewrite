const { models } = require('../../../sequelize');
const { serializeSingleton, getIdParam, createAndUpdate, findOne } = require('../../helpers');

const model = models.configuration
const relModels = []

async function getAll(req, res) {
  let formatted = []
  let where = {}

  if (req.query.key) {
    where["key"] = req.query.key
  }

  const instances = await model.findAll({
    where: where,
    include: relModels
  });

  for (const instance of instances) {
    let one = {}
    one["type"] = "configuration"
    one["id"] = instance.id
    one["key"] = instance.key
    one["type"] = instance.type
    // TODO: Enums
    switch (instance.type) {
      case "boolean":
        one["value"] = (instance.value.toLowerCase() === 'true');
        break
      case "integer":
        one["value"] = parseInt(instance.value)
        break
      case "string":
        one["value"] = instance.value
        break
    }
    formatted.push(one)
  }

  res.status(200).json(formatted);
}

async function getById(req, res) {
  const id = getIdParam(req);
  const instances = await findOne(id, model, relModels)
  if (instances) {
    res.status(200).json(serializeSingleton(instance, model, id));
  } else {
    res.status(404).send('404 - Not found');
  }
}

async function create(req, res) {
  if (req.params.id) {
    res.status(400).send(`Bad request: ID should not be provided, since it is determined automatically by the database.`)
  } else {
    let instance = await createAndUpdate(req, model, relModels)
    res.status(201).json(serializeSingleton(instance, model, instance["dataValues"]["id"]))
  }
}

async function update(req, res) {
  let instance = await createAndUpdate(req, model, relModels)
  if (instance) {
    res.status(201).json(serializeSingleton(instance, model, instance["dataValues"]["id"]))
  } else {
    res.status(404).send('404 - Not found');
  }
}

async function remove(req, res) {
  const id = getIdParam(req);

  let instance = await findOne(id, model, relModels)
  await instance.destroy();
  res.status(200).end();
};

module.exports = {
  getAll,
  getById,
  create,
  update,
  remove
};
