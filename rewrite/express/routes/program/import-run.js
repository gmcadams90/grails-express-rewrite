const { models } = require('../../../sequelize');
const { Sequelize } = require('sequelize');
const { serialize, serializeSingleton, getIdParam, createAndUpdate, findOne, sendMail } = require('../../helpers');
const config = require('../../config')

const model = models.import_run
const relModels = [models.training_location, models.ea_user, models.import_error]

// TODO: Enum this
const EMAIL_TRIGGERS = ["succeeded", "failed", "errors" ]

const emailBaseUrl = config.userImport.email.baseUrl
const emailSubject = config.userImport.email.subject

async function getAll(req, res) {
  let where = {}

  if (req.query.trainingLocationId) {
    where["trainingLocationId"] = req.query.trainingLocationId
  }

  if (req.query.userId) {
    where["eaUserId"] = req.query.userId
  }

  if (req.query.status) {
    where["status"] = req.query.status
  }

  if (req.query.type) {
    where["type"] = req.query.type
  }

  const instances = await model.findAll({
    where: where,
    include: relModels,
    order: Sequelize.literal('createdAt DESC')
  });
  res.status(200).json(serialize(instances, model));
}

async function getById(req, res) {
  const id = getIdParam(req);
  const instances = await findOne(id, model, relModels)
  if (instances) {
    res.status(200).json(serializeSingleton(instance, model, id));
  } else {
    res.status(404).send('404 - Not found');
  }
}

async function create(req, res) {
  if (req.params.id) {
    res.status(400).send(`Bad request: ID should not be provided, since it is determined automatically by the database.`)
  } else {
    let instance = await createAndUpdate(req, model, relModels)
    if (EMAIL_TRIGGERS.includes(instance.status)) {
      await sendEmail(instance)
    }
    res.status(201).json(serializeSingleton(instance, model, instance["dataValues"]["id"]))
  }
}

async function sendEmail(importRun) {
  let eaUser = importRun.getEa_user()

  if (!eaUser || !eaUser.email) {
    throw new Error("Attempting to send ImportRun status email, but eaUser cannot be located, or eaUser has no email.")
  } else {
    let targetUrl = emailBaseUrl + importRun.id

    await sendMail(eaUser.email,
      emailSubject,
      "/importRun/message",
      {importRun: importRun, importRunLink: targetUrl})
  }
}

// TODO: May need to add additional deserialize logic here from incoming body
async function update(req, res) {
  let instance = await createAndUpdate(req, model, relModels)
  if (instance) {
    res.status(201).json(serializeSingleton(instance, model, instance["dataValues"]["id"]))
  } else {
    res.status(404).send('404 - Not found');
  }
}

async function remove(req, res) {
  //do nothing
};

module.exports = {
  getAll,
  getById,
  create,
  update,
  remove
};
