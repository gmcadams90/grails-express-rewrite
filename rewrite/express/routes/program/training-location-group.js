const { models } = require('../../../sequelize');
const { serialize, serializeSingleton, getIdParam, createAndUpdate, findOne } = require('../../helpers');

const model = models.training_location_group
const relModels = [models.training_location]

async function getAll(req, res) {
  let where = {}

  if (req.query.name) {
    where['name'] = req.query.name
  }
  // TODO: Handle any other included parameters -- Is this really needed though?
  /*else {
  }*/

  const instances = await model.findAll({
    where: where,
    include: relModels
  });
  res.status(200).json(serialize(instances, model));
}

async function getById(req, res) {
  const id = getIdParam(req);
  const instances = await findOne(id, model, relModels)
  if (instances) {
    res.status(200).json(serializeSingleton(instances, model, id));
  } else {
    res.status(404).send('404 - Not found');
  }
}

async function create(req, res) {
  if (req.params.id) {
    res.status(400).send(`Bad request: ID should not be provided, since it is determined automatically by the database.`)
  } else {
    let instance = await createAndUpdate(req, model, relModels)
    res.status(201).json(serializeSingleton(instance, model, instance["dataValues"]["id"]))
  }
}

async function update(req, res) {
  let instance = await createAndUpdate(req, model, relModels)
  if (instance) {
    res.status(201).json(serializeSingleton(instance, model, instance["dataValues"]["id"]))
  } else {
    res.status(404).send('404 - Not found');
  }
}

async function remove(req, res) {
  const id = getIdParam(req);

  let tlg = await findOne(id, model, relModels)
  let tls = await tlg.getTrainingLocations()
  tls.forEach(tl => tlg.removeTrainingLocation(tl));
  await tlg.destroy();
  res.status(200).end();
};

module.exports = {
  getAll,
  getById,
  create,
  update,
  remove
};
