const { models } = require('../../../sequelize');
const { serialize, serializeSingleton, getIdParam, createAndUpdate, findOne, handleMissingParameter } = require('../../helpers');

const model = models.ica_course_group
const relModels = [models.course, models.ica_license]

async function getAll(req, res) {
  let where = {}

  if (req.query.code) {
    where["code"] = req.query.code
  }

  const instances = await model.findAll({
    where: where,
    include: relModels
  });
  res.status(200).json(serialize(instances, model));
}

async function getById(req, res) {
  const id = getIdParam(req);
  const instances = await findOne(id, model, relModels)
  if (instances) {
    res.status(200).json(serializeSingleton(instance, model, id));
  } else {
    res.status(404).send('404 - Not found');
  }
}

async function create(req, res) {
  if (req.params.id) {
    res.status(400).send(`Bad request: ID should not be provided, since it is determined automatically by the database.`)
  } else {
    let instance = await createAndUpdate(req, model, relModels)

    // Send to LMS
    try {
      // TODO: Do icaCourseGroupService
      //icaCourseGroupService.createCourseGroup(instance)
    } catch (e) {
      console.error("LMS Exception during IcaCourseGroup creation: " + e)

      // Delete on LMS error
      let rels = await instance.getCourses()
      rels.forEach(rel => instance.removeCourse(rel));
      rels = await instance.getIca_licenses()
      rels.forEach(rel => instance.removeIca_license(rel));
      await instance.destroy();

      throw e
    }

    res.status(201).json(serializeSingleton(instance, model, instance["dataValues"]["id"]))
  }
}

// TODO: Incorporate this into createAndUpdate OR replace createAndUpdate completely with this
async function addCoursesToCourseGroup(req, instance) {
  let propsJson = req.JSON.data
  let courses = []

  if (propsJson.relationships.courses.data) {
    courses = propsJson.relationships.courses.data
  } else {
    return handleMissingParameter("propsJson.relationships.courses.data")
  }

  for (const course of courses) {
    let courseInstance = await models.course.findOne({ where: { id: course.id} })
    if (courseInstance) {
      courseInstance.addIca_course_group(instance)
    } else {
      console.warn("Unable to find course " + course.id + "; not adding IcaCourseGroup association")
    }
  }
}

// TODO: Test this
async function update(req, res) {
  let instance = await createAndUpdate(req, model, relModels)
  if (instance) {
    try {
      if (instance.changed()) {
        // Send to LMS on change
        console.info("Updating ICA Course Group")
        // TODO: Add this service
        //icaCourseGroupService.updateCourseGroup(instance)
      }
    } catch (e) {
      console.error("LMS Exception during IcaCourseGroup update: " + e.message)
      throw e
    }
    res.status(201).json(serializeSingleton(instance, model, instance["dataValues"]["id"]))
  } else {
    res.status(404).send('404 - Not found');
  }
}

async function remove(req, res) {
  const id = getIdParam(req);

  let instance = await findOne(id, model, relModels)
  let rels = await instance.getIca_licenses()

  // Don't delete if we have assignments
  if (rels.length > 0) {
    return res.status(422).send(`Cannot delete ${instance.id}, ${rels.length} assignments are active`)
  }

  // Send to LMS
  try {
    // TODO: This service
    //icaCourseGroupService.deleteCourseGroup(instance)
  } catch (e) {
    console.error("LMS Exception during IcaCourseGroup delete: " + e.message)
    // Note: All cleared courses above will be auto-rolled back after throwing here
    throw e
  }

  rels.forEach(rel => instance.removeIca_license(rel));
  rels = await instance.getCourses()
  rels.forEach(rel => instance.removeCourse(rel));
  await instance.destroy();
  res.status(200).end();
}

module.exports = {
  getAll,
  getById,
  create,
  update,
  remove
};
