const { models } = require('../../../sequelize');
const { serialize, serializeSingleton, getIdParam, createAndUpdate, findOne } = require('../../helpers');
const moment = require('moment')

const model = models.ica_license
const relModels = [models.assigned_ica_license, models.ea_user, models.training_location, models.ica_course_group]

async function getAll(req, res) {
  let where = {}

  if (req.query.trainingLocationId) {
    where["trainingLocationId"] = req.query.trainingLocationId
  }

  if (req.query.icaCourseGroupId) {
    where["icaCourseGroupId"] = req.query.icaCourseGroupId
  }

  const instances = await model.findAll({
    where: where,
    include: relModels
  });
  res.status(200).json(serialize(instances, model));
}

async function getById(req, res) {
  const id = getIdParam(req);
  const instances = await findOne(id, model, relModels)
  if (instances) {
    res.status(200).json(serializeSingleton(instance, model, id));
  } else {
    res.status(404).send('404 - Not found');
  }
}

async function create(req, res) {
  if (req.params.id) {
    res.status(400).send(`Bad request: ID should not be provided, since it is determined automatically by the database.`)
  } else {
    let instance = await createAndUpdate(req, model, relModels)
    // Get license
    try {
      let traverseMap = buildAssignedTraverseMap(instance.getIca_course_group().itemCode, 1)
      // TODO: Do traverseService
      //let traverseResp = traverseService.traverseAssignLicenses(traverseMap, instance.trainingLocation)
      //instance.license = traverseResp["items"][0]["licenses"][0]["licenseKey"]
    } catch (e) {
      console.error(e?.message)
      console.debug("Using items " + e.traverseItemsMap.toString())

      // Delete
      let rels = await instance.getAssigned_ica_licenses()
      rels.forEach(rel => instance.removeAssigned_ica_license(rel));
      await instance.destroy();

      throw e
    }

    instance.dateEnded = moment(instance.dateCreated).add(1, 'year').toDate()

    res.status(201).json(serializeSingleton(instance, model, instance["dataValues"]["id"]))
  }
}

async function update(req, res) {
  let instance = await createAndUpdate(req, model, relModels)
  if (instance) {
    res.status(201).json(serializeSingleton(instance, model, instance["dataValues"]["id"]))
  } else {
    res.status(404).send('404 - Not found');
  }
}

async function remove(req, res) {
  const id = getIdParam(req);

  let instance = await findOne(id, model, relModels)
  let rels = await instance.getAssigned_ica_licenses()
  rels.forEach(rel => instance.removeAssigned_ica_license(rel));
  await instance.destroy();
  res.status(200).end();
}

async function buildAssignedTraverseMap(itemCode, licenseCount = 0) {
  let items = []
  let jsonMap = {}
  let courseMap = {}

  courseMap['itemCode'] = itemCode
  courseMap['licenseCountToAssign'] = licenseCount
  items.push(courseMap)

  jsonMap['items'] = items

  return jsonMap
}

module.exports = {
  getAll,
  getById,
  create,
  update,
  remove
};
