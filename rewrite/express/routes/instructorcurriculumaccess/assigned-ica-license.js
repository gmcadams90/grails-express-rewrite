const { models } = require('../../../sequelize');
const { serialize, serializeSingleton, getIdParam, createAndUpdate, findOne } = require('../../helpers');

const model = models.assigned_ica_license
const relModels = [models.moodle_user, models.ea_user, models.ica_license]

async function getAll(req, res) {
  let where = {}

  if (req.query.moodleUserId) {
    where["moodleUserId"] = req.query.moodleUserId
  }

  if (req.query.icaLicenseId) {
    where["icaLicenseId"] = req.query.icaLicenseId
  }

  const instances = await model.findAll({
    where: where,
    include: relModels
  });
  res.status(200).json(serialize(instances, model));
}

async function getById(req, res) {
  const id = getIdParam(req);
  const instances = await findOne(id, model, relModels)
  if (instances) {
    res.status(200).json(serializeSingleton(instance, model, id));
  } else {
    res.status(404).send('404 - Not found');
  }
}

async function create(req, res) {
  if (req.params.id) {
    res.status(400).send(`Bad request: ID should not be provided, since it is determined automatically by the database.`)
  } else {
    try {
      // TODO: Do icaCourseGroupService
      //icaCourseGroupService.assignCourseGroups(instance)
    } catch (e) {
      console.error("LMS Exception during AssignedIcaLicense assign: " + e.message)
      throw e
    }
    let instance = await createAndUpdate(req, model, relModels)
    res.status(201).json(serializeSingleton(instance, model, instance["dataValues"]["id"]))
  }
}

async function update(req, res) {
  let instance = await createAndUpdate(req, model, relModels)
  if (instance) {
    if (instance.dateClosed != null) {
      try {
        // TODO: Do icaCourseGroupService
        //icaCourseGroupService.unassignCourseGroups(instance)
      } catch (e) {
        console.error("LMS Exception during AssignedIcaLicense unassign: " + e.message)
      }
    }
    res.status(201).json(serializeSingleton(instance, model, instance["dataValues"]["id"]))
  } else {
    res.status(404).send('404 - Not found');
  }
}

async function remove(req, res) {
  const id = getIdParam(req);

  let instance = await findOne(id, model, relModels)
  await instance.destroy();
  res.status(200).end();
};

module.exports = {
  getAll,
  getById,
  create,
  update,
  remove
};
