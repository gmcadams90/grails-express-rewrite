const { models } = require('../../../sequelize');
const { serialize, serializeSingleton, getIdParam, createAndUpdate, findOne, handleMissingParameter } = require('../../helpers');

const model = models.pending_enrollment
const relModels = [models.training_location, models.cart_item, models.ea_user, models.course_session]

async function getAll(req, res) {
  let where = {}

  if (req.query.trainingLocationId) {
    where['trainingLocationId'] = req.query.trainingLocationId
  } else {
    return res.status(422).json(handleMissingParameter('trainingLocationId'))
  }
  if (req.query.courseSessionId) {
    where['courseSessionId'] = req.query.courseSessionId
  }

  const instances = await model.findAll({
    where: where,
    include: relModels
  });
  // TODO: After service
  //def jsonMeta = MetaDataBuilderService.getDomainClassesServiceMetaData(instances, pendingEnrollmentService)
  let multipart = {"pendingEnrollments": instances}//, "jsonMeta": jsonMeta}
  res.status(200).json(serialize(multipart, model));
}

async function getById(req, res) {
  const id = getIdParam(req);
  const instance = await findOne(id, model, relModels)
  if (instance) {
    // TODO: After service
    //def jsonMeta = MetaDataBuilderService.getDomainClassesServiceMetaData(instance, pendingEnrollmentService)
    let multipart = {"pendingEnrollments": instance}//, "jsonMeta": jsonMeta}
    res.status(200).json(serializeSingleton(instance, model, id));
  } else {
    res.status(404).send('404 - Not found');
  }
}

async function create(req, res) {
  if (req.params.id) {
    res.status(400).send(`Bad request: ID should not be provided, since it is determined automatically by the database.`)
  } else {
    let instance = await createAndUpdate(req, model, relModels)
    res.status(201).json(serializeSingleton(instance, model, instance["dataValues"]["id"]))
  }
}

async function update(req, res) {
  let instance = await createAndUpdate(req, model, relModels)
  if (instance) {
    res.status(201).json(serializeSingleton(instance, model, instance["dataValues"]["id"]))
  } else {
    res.status(404).send('404 - Not found');
  }
}

async function remove(req, res) {
  const id = getIdParam(req);

  let pe = await findOne(id, model, relModels)
  await pe.destroy();
  res.status(200).end();
};

module.exports = {
  getAll,
  getById,
  create,
  update,
  remove
};
