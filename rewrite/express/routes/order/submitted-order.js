const { models } = require('../../../sequelize');
const { Op, Sequelize, fn, col } = require('sequelize')
const { serialize, serializeSingleton, getIdParam, createAndUpdate, findOne, handleMissingParameter } = require('../../helpers');

const model = models.submitted_order
const relModels = [models.training_location, {model: models.submitted_order_item}, models.ea_user]

async function getAll(req, res) {
  let where = {}
  let sort = {}

  if (req.query.sort) {
    let sortString = req.query.sort.slice(1, -2)
    sortString = sortString.split(',')
    for (const entry of sortString) {
      let pair = entry.split(':')
      sort[pair[0]] = pair[1]
    }
  }
  let tljatc = req.query.trainingLocationJatc
  let ord = req.query.order ? req.query.order : "desc"
  let ordBy = req.query.orderBy ? req.query.orderBy : "dateCreated"

  let admin
  if (req.query.admin) {
    admin = true
  } else {
    admin = false
  }

  let offset = req.query.offset ? req.query.offset.toInteger() * req.query.max.toInteger() : 0
  let beginDate = null
  let endDate = null

  if (req.query.begin_date) {
    let date = req.query.begin_date.replaceAll("\\B'\\b|\\b'\\B", "")
    date = date.replaceAll('\\B"\\b|\\b"\\B', "")
    beginDate = new Date(date)
  }
  if (req.query.end_date) {
    let date = req.query.end_date.replaceAll("\\B'\\b|\\b'\\B", "")
    date = date.replaceAll('\\B"\\b|\\b"\\B', "")
    endDate = new Date(date)
  }

  if (tljatc && beginDate) {
    if (admin) {
      relModels[1]["where"][Op.like] = {"jatc": '%' + tljatc + '%'}
      where[Op.and] = [
        {
          dateCreated: {
            [Op.gte]: beginDate
          }
        }
      ]
      if (endDate) {
        where[Op.and].push(
          {
            dateCreated: {
              [Op.lte]: endDate
            }
          }
        )
      }
    } else {
      relModels[1]["where"][Op.eq] = {'jatc': tljatc}
      where[Op.and] = [
        {
          dateCreated: {
            [Op.gte]: beginDate
          }
        }
      ]
      if (endDate) {
        where[Op.and].push(
          {
            dateCreated: {
              [Op.lte]: endDate
            }
          }
        )
      }
    }
  } else if (tljatc) {
    if (admin) {
      relModels[1]["where"][Op.like] = {"jatc": '%' + tljatc + '%'}
    } else {
      relModels[1]["where"][Op.eq] = {"jatc": tljatc}
    }
    if (endDate) {
      where[Op.le] = [
        {
          dateCreated: endDate
        }
      ]
    }
  } else if (beginDate) {
      where[Op.gte] = [
        {
          dateCreated: beginDate
        }
      ]
    if (endDate) {
      where[Op.lte] = [
        {
          dateCreated: endDate
        }
      ]
    }
  } else if (endDate) {
      where[Op.lte] = [
        {
          dateCreated: endDate
        }
      ]
  }

  if (!req.query.withDeleted) {
    where['deleted'] = false
  }

  const instances = await model.findAll({
    attributes: {
      include: ['id', 'dateCreated', [fn("COUNT", col("submitted_order.id")), "count"]]
    },
    subQuery: false,
    group: ['id'],
    limit: req.query.max,
    offset: offset,
    order: Sequelize.literal(ordBy + " " + ord),
    where: where,
    include: relModels
  });

  res.status(200).json(serialize(instances, model));
}

async function getById(req, res) {
  const id = getIdParam(req);
  const instance = await findOne(id, model, relModels)
  if (instance) {
    res.status(200).json(serializeSingleton(instance, model, id));
  } else {
    res.status(404).send('404 - Not found');
  }
}

// TODO: Compare with Grails; might need to do extra extraction here
async function create(req, res) {
  if (req.params.id) {
    res.status(400).send(`Bad request: ID should not be provided, since it is determined automatically by the database.`)
  } else {
    let instance = await createAndUpdate(req, model, relModels)
    res.status(201).json(serializeSingleton(instance, model, instance["dataValues"]["id"]))
  }
}

async function update(req, res) {
  let instance = await createAndUpdate(req, model, relModels)
  if (instance) {
    res.status(201).json(serializeSingleton(instance, model, instance["dataValues"]["id"]))
  } else {
    res.status(404).send('404 - Not found');
  }
}

async function remove(req, res) {
  const id = getIdParam(req);

  let instance = await findOne(id, model, relModels)
  instance.deleted = true
  await instance.save()
  /*let rels = await instance.getSubmitted_order_items()
  rels.forEach(rel => instance.removeSubmitted_order_item(rel));
  await instance.destroy();*/
  res.status(200).end();
};

module.exports = {
  getAll,
  getById,
  create,
  update,
  remove
};
