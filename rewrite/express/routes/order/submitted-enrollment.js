const { models } = require('../../../sequelize');
const { serialize, serializeSingleton, getIdParam, createAndUpdate, findOne, handleMissingParameter } = require('../../helpers');

const model = models.submitted_enrollment
const relModels = [models.course_session, models.training_location, models.submitted_order_item]

async function getAll(req, res) {
  let where = {}

  if (req.query.trainingLocationId) {
    where["trainingLocationId"] = req.query.trainingLocationId
  } else if (req.query["filter[id]"]) {
    let ids = req.query["filter[id]"].split(',')
    where["id"] = ids
  } else {
    return res.status(422).json(handleMissingParameter(["trainingLocationId", "filter[id]"]))
  }

  const instances = await model.findAll({
    where: where,
    include: relModels
  });
  res.status(200).json(serialize(instances, model));
}

async function getById(req, res) {
  const id = getIdParam(req);
  const instance = await findOne(id, model, relModels)
  if (instance) {
    res.status(200).json(serializeSingleton(instance, model, id));
  } else {
    res.status(404).send('404 - Not found');
  }
}

// TODO: Test/Compare with Grails
async function create(req, res) {
  if (req.params.id) {
    res.status(400).send(`Bad request: ID should not be provided, since it is determined automatically by the database.`)
  } else {
    let instance = await createAndUpdate(req, model, relModels)
    res.status(201).json(serializeSingleton(instance, model, instance["dataValues"]["id"]))
  }
}

async function update(req, res) {
  let instance = await createAndUpdate(req, model, relModels)
  if (instance) {
    res.status(201).json(serializeSingleton(instance, model, instance["dataValues"]["id"]))
  } else {
    res.status(404).send('404 - Not found');
  }
}

async function remove(req, res) {
  const id = getIdParam(req);

  let se = await findOne(id, model, relModels)
  await se.destroy();
  res.status(200).end();
};

module.exports = {
  getAll,
  getById,
  create,
  update,
  remove
};
