const { models } = require('../../../sequelize');
const { serialize, serializeSingleton, getIdParam, createAndUpdate, findOne } = require('../../helpers');

const model = models.cart_item
const relModels = [models.training_location, models.ea_user]//, models.course_session

async function getAll(req, res) {
  let where = {"deleted": false}

  if (req.query.trainingLocationId) {
    where['trainingLocationId'] = req.query.trainingLocationId
  }

  if (req.query.courseSessionId) {
    where['courseSessionId'] = req.query.courseSessionId
  }

  const instances = await model.findAll({
    where: where,
    include: relModels
  });
  // TODO: Add this service/method
  //cartItemService.clearBadPendingEnrollments(instances)
  //let jsonMeta = MetaDataBuilderService.getDomainClassesServiceMetaData(instances, cartItemService)

  let out = {"cartItemList": serialize(instances, model)}//, "jsonMeta": jsonMeta}
  res.status(200).json(out);
}

async function getById(req, res) {
  const id = getIdParam(req);
  const instance = await findOne(id, model, relModels)
  if (instance) {
    //cartItemService.clearBadPendingEnrollments(instance)
    //let jsonMeta = MetaDataBuilderService.getDomainClassServiceMetaData(instance, cartItemService)
    let out = {"cartItem": serializeSingleton(instance, model, id)}//, 'jsonMeta': jsonMeta}
    res.status(200).json(out);
  } else {
    res.status(404).send('404 - Not found');
  }
}

async function create(req, res) {
  if (req.params.id) {
    res.status(400).send(`Bad request: ID should not be provided, since it is determined automatically by the database.`)
  } else {
    // TODO: Update all this cart item service is created
    //let propsJson = instance.toJson()
    //let pendingEnrollments = jsonDataExtractorService.extractAndRemoveJsonArrayasArrayListFromRelationships(propsJson, "pendingEnrollments")
    //let eaUserData = jsonDataExtractorService.extractAndRemoveJsonObjectFromRelationships(propsJson, "eaUser")
    //let eaUser = await findOne(eaUserData.id, relModels[1], [])
    //let courseSessionData = jsonDataExtractorService.extractAndRemoveJsonObjectFromRelationships(propsJson, "courseSession")
    //let courseSession = await findOne(courseSessionData.id, relModels[2], [])
    /*if (courseSession.getCourse_group_session()) {
      let respArr = cartItemService.addToOrCreateCartItems(pendingEnrollments, eaUser, courseSession)
      res.status(201).json(respArr)
    } else {
      let instance = cartItemService.addToOrCreateCartItem(pendingEnrollments, eaUser, courseSession)
      res.status(201).json(serializeSingleton(instance, model, attrs, rels, instance["dataValues"]["id"]))
    }*/
  }
}

async function update(req, res) {
  let instance = await createAndUpdate(req, model, relModels)
  if (instance) {
    // def propsJson = instance.toJson()
    // let pendingEnrollments = jsonDataExtractorService.extractAndRemoveJsonArrayasArrayListFromRelationships(propsJson, "pendingEnrollments")
    //cartItemService.updateCartItem(instance, pendingEnrollments)
    res.status(201).json(serializeSingleton(instance, model, instance["dataValues"]["id"]))
  } else {
    res.status(404).send('404 - Not found');
  }
}

async function remove(req, res) {
  const id = getIdParam(req);

  let instance = await findOne(id, model, relModels)
  instance.deleted = true
  await instance.save()

  /*let rels = await ci.getPending_enrollments()
  rels.forEach(rel => instance.removePending_enrollment(rel));
  await instance.destroy();*/
  res.status(200).end();
};

module.exports = {
  getAll,
  getById,
  create,
  update,
  remove
};
