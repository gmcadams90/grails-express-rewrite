const { Op, Sequelize, fn, col } = require('sequelize')
const { models } = require('../../../sequelize');
const { serialize, serializeSingleton, getIdParam, createAndUpdate, findOne } = require('../../helpers');

const model = models.completion_transaction
const relModels = [models.training_location, models.ea_user, models.course_completion]

async function getAll(req, res) {
  let where = {}
  let ord = req.query.order ? req.query.order : "desc"
  let ordBy = req.query.orderBy ? req.query.orderBy : "dateCreated"
  let max = req.query.max ? req.query.max.toInteger() : 0
  let offset = req.query.offset ? req.query.offset.toInteger() * max : 0
  let beginDate
  let endDate

  if (req.query.begin_date) {
    let date = req.query.begin_date.replaceAll("\\B'\\b|\\b'\\B", "")
    date = date.replaceAll('\\B"\\b|\\b"\\B', "")
    beginDate = new Date(date)
  }

  if (req.query.end_date) {
    let date = req.query.end_date.replaceAll("\\B'\\b|\\b'\\B", "")
    date = date.replaceAll('\\B"\\b|\\b"\\B', "")
    endDate = new Date(date)
  }

  if (req.query.firstName) {
    req.query.tmsFirstName = req.query.firstName
  }

  if (req.query.lastName) {
    req.query.tmsLastName = req.query.lastName
  }

  // Query
  if (req.query.trainingLocationId) {
    where['trainingLocationId'] = req.query.trainingLocationId
  }
  if (req.query.tmsUserName) {
    let eaUsers = await models.ea_user.findAll({
      where: {
        [Op.or]: [
          {
            firstName: {
              [Op.like]: '%' + req.query.lmsUserName + '%'
            }
          },
          {
            lastName: {
              [Op.like]: '%' + req.query.lmsUserName + '%'
            }
          }
        ]
      }
    })
  }
  if (req.query.tmsFirstName || req.query.tmsLastName) {
    let eaUser

    if (req.query.tmsLastName && req.query.tmsFirstName) {
      eaUser = await models.ea_user.findAll({
        where: {
          [Op.and]: [
            {
              firstName: {
                [Op.like]: '%' + req.query.tmsFirstName + '%'
              }
            },
            {
              lastName: {
                [Op.like]: '%' + req.query.tmsLastName + '%'
              }
            }
          ]
        }
      })
    }
    else if (req.query.tmsLastName) {
      eaUser = await models.ea_user.findAll({
        where: {'lastName': {$like: '%' + req.query.tmsLastName + '%'}}
      });
    }
    else if (req.query.tmsFirstName) {
      eaUser = await models.ea_user.findAll({
        where: {'firstName': {$like: '%' + req.query.tmsFirstName + '%'}}
      });
    }

    where['eaUserId'][Op.in] = [eaUser.ids]
  }
  if (req.query.beginDate) {
    where['dateCreated'][Op.gt] = new Date(req.query.beginDate)
  }
  if (req.query.endDate) {
    where['dateCreated'][Op.lte] = new Date(req.query.endDate)
  }

  const instances = await model.findAll({
    attributes: {
      include: ['id', 'dateCreated', [fn("COUNT", col("ebook_transaction.id")), "count"]]
    },
    subQuery: false,
    group: ['id'],
    limit: max,
    offset: offset,
    order: Sequelize.literal(ordBy + " " + ord),
    where: where,
    include: relModels
  });
  res.status(200).json(serialize(instances, model));
}

async function getById(req, res) {
  const id = getIdParam(req);
  const instance = await findOne(id, model, relModels)
  if (instance) {
    res.status(200).json(serializeSingleton(instance, model, id));
  } else {
    res.status(404).send('404 - Not found');
  }
}

async function create(req, res) {
  if (req.params.id) {
    res.status(400).send(`Bad request: ID should not be provided, since it is determined automatically by the database.`)
  } else {
    let instance = await createAndUpdate(req, model, relModels)
    res.status(201).json(serializeSingleton(instance, model, instance["dataValues"]["id"]))
  }
}

async function update(req, res) {
  let instance = await createAndUpdate(req, model, relModels)
  if (instance) {
    res.status(201).json(serializeSingleton(instance, model, instance["dataValues"]["id"]))
  } else {
    res.status(404).send('404 - Not found');
  }
}

async function remove(req, res) {
  const id = getIdParam(req);

  let ct = await findOne(id, model, relModels)
  let ccs = await ct.getCourse_completions()
  ccs.forEach(cc => ct.removeCourse_completion(cc));
  await ct.destroy();
  res.status(200).end();
};

module.exports = {
  getAll,
  getById,
  create,
  update,
  remove
};
