const { Op, Sequelize, fn, col } = require('sequelize')
const { models } = require('../../../sequelize');
const { serialize, serializeSingleton, getIdParam, createAndUpdate, findOne } = require('../../helpers');

const model = models.ebook_transaction
const relModels = [models.training_location, models.ea_user, models.moodle_user, models.assigned_ebook, models.ebook_transaction_group]

async function getAll(req, res) {
  let where = {}
  let sort = {}

  if (req.query.ebookTransactionGroupId) {
    where['ebookTransactionGroupId'] = req.query.ebookTransactionGroupId
    const instances = await model.findAll({
      where: where,
      include: relModels
    });
    res.status(200).json(serialize(instances, model));
  } else {
    if (req.query.sort) {
      let sortString = req.query.sort.slice(1, -2)
      sortString = sortString.split(',')
      for (const entry of sortString) {
        let pair = entry.split(':')
        sort[pair[0]] = pair[1]
      }
    }

    let ord = req.query.order ? req.query.order : "DESC"
    let ordBy = req.query.orderBy ? req.query.orderBy : "dateCreated"
    let max = req.query.max ? req.query.max.toInteger() : 0
    let offset = req.query.offset ? req.query.offset.toInteger() * max : 0

    let beginDate = null
    let endDate = null

    if (req.query.begin_date) {
      let date = req.query.begin_date.replaceAll("\\B'\\b|\\b'\\B", "")
      date = date.replaceAll('\\B"\\b|\\b"\\B', "")
      beginDate = new Date(date)
    }
    if (req.query.end_date) {
      let date = req.query.end_date.replaceAll("\\B'\\b|\\b'\\B", "")
      date = date.replaceAll('\\B"\\b|\\b"\\B', "")
      endDate = new Date(date)
    }

    if (req.query.itemCode) {
      where["itemCode"] = req.query.itemCode
    }
    if (req.query.moodleUserId) {
      where["moodleUserId"] = req.query.moodleUserId
    }
    if (req.query.trainingLocationId) {
      where["trainingLocationId"] = req.query.trainingLocationId
    }
    if (req.query.status) {
      where["status"] = req.query.status.toLowerCase()
    }
    if (req.query.itemCode) {
      where["itemCode"] = req.query.itemCode
    }
    if (req.query.firstName || req.query.lastName) {
      let firstNameEaUser
      let lastNameEaUser

      if (req.query.firstName) {
        let eaUser = await models.ea_user.findAll({
          where: {'firstName': {$like: '%' + req.query.firstName + '%'}}
        });
        firstNameEaUser = eaUser.id
      }
      if (req.query.lastName) {
        let eaUser = await models.ea_user.findAll({
          where: {'firstName': {$like: '%' + req.query.firstName + '%'}}
        });
        lastNameEaUser = eaUser.id
      }

      where['eaUserId'][Op.or] = [firstNameEaUser, lastNameEaUser]
    }
    if (req.query.beginDate) {
      where['dateCreated'][Op.gt] = new Date(req.query.beginDate)
    }
    if (req.query.endDate) {
      where['dateCreated'][Op.lte] = new Date(req.query.endDate)
    }

    const instances = await model.findAll({
      attributes: {
        include: ['id', 'dateCreated', [fn("COUNT", col("ebook_transaction.id")), "count"]]
      },
      subQuery: false,
      group: ['id'],
      limit: max,
      offset: offset,
      order: Sequelize.literal(ordBy + " " + ord),
      where: where,
      include: relModels
    });
    res.status(200).json(serialize(instances, model));
  }
}

async function getById(req, res) {
  const id = getIdParam(req);
  const instance = await findOne(id, model, relModels)
  if (instance) {
    res.status(200).json(serializeSingleton(instance, model, id));
  } else {
    res.status(404).send('404 - Not found');
  }
}

async function create(req, res) {
  if (req.params.id) {
    res.status(400).send(`Bad request: ID should not be provided, since it is determined automatically by the database.`)
  } else {
    let instance = await createAndUpdate(req, model, relModels)
    res.status(201).json(serializeSingleton(instance, model, instance["dataValues"]["id"]))
  }
}

async function update(req, res) {
  let instance = await createAndUpdate(req, model, relModels)
  if (instance) {
    res.status(201).json(serializeSingleton(instance, model, instance["dataValues"]["id"]))
  } else {
    res.status(404).send('404 - Not found');
  }
}

async function remove(req, res) {
  const id = getIdParam(req);

  let ebt = await findOne(id, model, relModels)
  await ebt.destroy();
  res.status(200).end();
};

module.exports = {
  getAll,
  getById,
  create,
  update,
  remove
};
