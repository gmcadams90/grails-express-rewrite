const { Op, Sequelize, fn, col } = require('sequelize')
const { models } = require('../../../sequelize');
const { serialize, serializeSingleton, getIdParam, createAndUpdate, findOne } = require('../../helpers');

const model = models.ebook_transaction_group
const relModels = [models.training_location, {model: models.ea_user}, {model: models.ebook_transaction}]

async function getAll(req, res) {
  let where = {}
  let ord = req.query.order ? req.query.order : "desc"
  let ordBy = req.query.orderBy ? req.query.orderBy : "dateCreated"
  let max = req.query.max ? parseInt(req.query.max) : 0
  let offset = req.query.offset ? parseInt(req.query.offset) * max : 0
  let beginDate
  let endDate

  if (req.query.begin_date) {
    let date = req.query.begin_date.replaceAll("\\B'\\b|\\b'\\B", "")
    date = date.replaceAll('\\B"\\b|\\b"\\B', "")
    beginDate = new Date(date)
  }

  if (req.query.end_date) {
    let date = req.query.end_date.replaceAll("\\B'\\b|\\b'\\B", "")
    date = date.replaceAll('\\B"\\b|\\b"\\B', "")
    endDate = new Date(date)
  }

  if (req.query.firstName) {
    req.query.tmsFirstName = req.query.firstName
  }

  if (req.query.lastName) {
    req.query.tmsLastName = req.query.lastName
  }

  // Query
  if (req.query.trainingLocationId) {
    where[Op.and] = [{trainingLocationId: req.query.trainingLocationId}]
  }
  if (req.query.status) {
    where[Op.and].push({status: req.query.status.toLowerCase()})
  }
  if (req.query.lmsUserName) {
    relModels[2][Op.or] = [
      {
        firstName: {
          [Op.like]: '%' + req.query.lmsUserName + '%'
        }
      },
      {
        lastName: {
          [Op.like]: '%' + req.query.lmsUserName + '%'
        }
      }
    ]
  }
  if (req.query.tmsUserName) {
    relModels[1][Op.or] = [
      {
        firstName: {
          [Op.like]: '%' + req.query.lmsUserName + '%'
        }
      },
      {
        lastName: {
          [Op.like]: '%' + req.query.lmsUserName + '%'
        }
      }
    ]
  }

  if (req.query.tmsFirstName || req.query.tmsLastName) {
    if (req.query.tmsFirstName) {
      relModels[1][Op.and] = [
        {
          firstName: {
            [Op.like]: '%' + req.query.tmsFirstName + '%'
          }
        }
      ]
    }
    if (req.query.tmsLastName) {
      relModels[1][Op.and].push(
        {
          lastName: {
            [Op.like]: '%' + req.query.tmsLastName + '%'
          }
        }
      )
    }
  }

  if (req.query.lmsFirstName || req.query.lmsLastName) {
    if (req.query.lmsFirstName) {
      relModels[2][Op.and] = [
        {
          firstName: {
            [Op.like]: '%' + req.query.lmsFirstName + '%'
          }
        }
      ]
    }
    if (req.query.lmsLastName) {
      relModels[2][Op.and].push(
        {
          lastName: {
            [Op.like]: '%' + req.query.lmsLastName + '%'
          }
        }
      )
    }
  }

  if (req.query.beginDate) {
    where['dateCreated'][Op.gte] = new Date(req.query.beginDate)
  }

  if (req.query.endDate) {
    where['dateCreated'][Op.lte] = new Date(req.query.endDate)
  }

  const count = await model.count({
    where: where,
    include: relModels
  });

  console.log(count)

  const instances = await model.findAll({
    attributes: {
      include: ['id', 'dateCreated']
    },
    limit: max,
    offset: offset,
    order: Sequelize.literal(ordBy + " " + ord),
    where: where,
    include: relModels
  });

  console.log(instances)

  res.status(200).json(serialize({ebookTransactionGroupCount: count, ebookTransactionGroupList: instances}, model));
}

async function getById(req, res) {
  const id = getIdParam(req);
  const instance = await findOne(id, model, relModels)
  if (instance) {
    res.status(200).json(serializeSingleton(instance, model, id));
  } else {
    res.status(404).send('404 - Not found');
  }
}

async function create(req, res) {
  if (req.params.id) {
    res.status(400).send(`Bad request: ID should not be provided, since it is determined automatically by the database.`)
  } else {
    let instance = await createAndUpdate(req, model, relModels)
    res.status(201).json(serializeSingleton(instance, model, instance["dataValues"]["id"]))
  }
}

async function update(req, res) {
  let instance = await createAndUpdate(req, model, relModels)
  if (instance) {
    res.status(201).json(serializeSingleton(instance, model, instance["dataValues"]["id"]))
  } else {
    res.status(404).send('404 - Not found');
  }
}

async function remove(req, res) {
  const id = getIdParam(req);

  let ebtg = await findOne(id, model, relModels)
  let ebts = await ebtg.getEbook_transactions()
  ebts.forEach(ebt => ebtg.removeEbook_transaction(ebt));
  await ebtg.destroy();
  res.status(200).end();
};

module.exports = {
  getAll,
  getById,
  create,
  update,
  remove
};
