const { Op } = require('sequelize');
const { models } = require('../../../sequelize');
const { serialize, serializeSingleton, getIdParam, createAndUpdate, findOne, sendMail } = require('../../helpers');
const { lmsRequestService } = require('../../services/lms-request-service')
const config = require('../../config')

const model = models.access_request
const relModels = [models.training_location, models.program_classification, models.filter_group, models.course]

const baseUrl = config.accessRequests.baseUrl
const frontEndpoint = config.accessRequests.frontEndpoint

async function getAll(req, res) {
  let where = {}

  if (req.query.isInstructor) {
    where['isInstructor'] = req.query.isInstructor
  }

  if (req.query.trainingLocationId) {
    where['trainingLocationId'] = req.query.trainingLocationId
  }

  if (req.query.status) {
    where['status'] = req.query.status
  }

  if (req.query.email) {
    where['email'] = req.query.email
  }

  if (!req.query.withDeleted) {
    where['deleted'] = false
  }

  const instances = await model.findAll({
    where: where,
    include: relModels
  });
  res.status(200).json(serialize(instances, model));
}

async function getById(req, res) {
  const id = getIdParam(req);
  const instances = await findOne(id, model, relModels)
  if (instances) {
    res.status(200).json(serializeSingleton(instance, model, id));
  } else {
    res.status(404).send('404 - Not found');
  }
}

async function create(req, res) {
  if (req.params.id) {
    res.status(400).send(`Bad request: ID should not be provided, since it is determined automatically by the database.`)
  } else {
    let instance = await createAndUpdate(req, model, relModels)
    if (instance.getTraining_location().subscriptionModelOn) {
      await sendEmails(instance)
    }
    res.status(201).json(serializeSingleton(instance, model, instance["dataValues"]["id"]))
  }
}

async function sendEmails(accessRequest) {
  let where = {}

  where["trainingLocationId"] = accessRequest.getTraining_location().id
  // TODO: Enum this
  where["role"][Op.or] = ['trainingDirectorAssistant', 'trainingDirector']
  where["active"] = true

  let eaUsers = await models.ea_user.findAll({
    where: where,
    attributes: "email"
  });

  let emails = []
  for (const eaUser of eaUsers) {
    emails.push(eaUser.email)
  }

  if (accessRequest.isInstructor) {
    await sendMail(emails,
      "etA Blended Learning LMS Access Request",
      "/accessRequest/instructorEmail",
      {accessRequest: accessRequest, trainingLocation: accessRequest.getTraining_location(), baseUrl: baseUrl, frontEndpoint: frontEndpoint })
  } else {
    let courses = await getRequiredCourseNames(accessRequest.getCourses())
    await sendMail(emails,
      "etA Blended Learning LMS Access Request",
      "/accessRequest/studentEmail",
      {accessRequest: accessRequest, trainingLocation: accessRequest.getTraining_location(), studentType: accessRequest.getStudent_type(), courses: courses, baseUrl: baseUrl, frontEndpoint: frontEndpoint })
  }
}

async function getRequiredCourseNames(courses) {
  let retCourses = []
  let lmsCourses = await getLmsCourseEnrollments()
  for (const course of courses) {
    let id = course.id
    if (id in lmsCourses) {
      retCourses.push(lmsCourses[id])
    }
  }
}

async function getLmsCourseEnrollments() {
  let resp = await lmsRequestService.makeLmsRequest("get_course_list", {})
  let respJson = JSON.parse(resp.body)
  let retMap = {}

  if (respJson.success && respJson.data) {
    for (const course of respJson.data) {
      retMap[course.id] = course
    }
  } else {
    throw new Error("Bad Request to get_course_list")
  }
  return retMap
}

async function update(req, res) {
  let instance = await createAndUpdate(req, model, relModels)
  if (instance) {
    res.status(201).json(serializeSingleton(instance, model, instance["dataValues"]["id"]))
  } else {
    res.status(404).send('404 - Not found');
  }
}

async function remove(req, res) {
  const id = getIdParam(req);

  let instance = await findOne(id, model, relModels)
  instance.deleted = true;
  await instance.save();
  /*let rels = await instance.getFilter_groups()
  rels.forEach(rel => instance.removeFilter_group(rel));
  rels = await instance.getCourses()
  rels.forEach(rel => instance.removeCourse(rel));
  await instance.destroy();*/
  res.status(200).end();
};

module.exports = {
  getAll,
  getById,
  create,
  update,
  remove
};
