const { models } = require('../../../sequelize');
const { serialize, serializeSingleton, getIdParam, createAndUpdate, findOne } = require('../../helpers');

const model = models.program_classification
const relModels = [models.student_type, models.access_request, models.moodle_user, {model: models.training_location}]

async function getAll(req, res) {
  if (req.query.trainingLocationId) {
    relModels[3]["where"] = {"id": req.query.trainingLocationId}
  } else if (req.query.jatc) {
    relModels[3]["where"] = {"jatc": req.query.jatc}
  }

  const instances = await model.findAll({
    include: relModels
  });
  res.status(200).json(serialize(instances, model));
}

async function getById(req, res) {
  const id = getIdParam(req);
  const instance = await findOne(id, model, relModels)
  if (instance) {
    res.status(200).json(serializeSingleton(instance, model, id));
  } else {
    res.status(404).send('404 - Not found');
  }
}

async function create(req, res) {
  if (req.params.id) {
    res.status(400).send(`Bad request: ID should not be provided, since it is determined automatically by the database.`)
  } else {
    let instance = await createAndUpdate(req, model, relModels)
    res.status(201).json(serializeSingleton(instance, model, instance["dataValues"]["id"]))
  }
}

async function update(req, res) {
  let instance = await createAndUpdate(req, model, relModels)
  if (instance) {
    res.status(201).json(serializeSingleton(instance, model, instance["dataValues"]["id"]))
  } else {
    res.status(404).send('404 - Not found');
  }
}

async function remove(req, res) {
  const id = getIdParam(req);

  let pc = await findOne(id, model, relModels)
  let ars = await pc.getAccess_requests()
  ars.forEach(ar => pc.removeAccess_request(ar));
  let mus = await pc.getMoodle_users()
  mus.forEach(mu => pc.removeMoodle_user(mu));
  pc.status(200).end();
};

module.exports = {
  getAll,
  getById,
  create,
  update,
  remove
};
