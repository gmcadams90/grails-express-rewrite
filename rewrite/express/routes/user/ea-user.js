const { models } = require('../../../sequelize');
const { serialize, serializeSingleton, getIdParam, createAndUpdate, findOne } = require('../../helpers');
const { removeTokens } = require('../../authentication')

const model = models.ea_user
const relModels = [{model: models.training_location},
  {model: models.permission_role, required: false},
  models.cart_item,
  models.custom_course,
  models.ebook_transaction,
  models.ebook_transaction_group,
  {model: models.user_type, required: false},
  models.pending_enrollment,
  models.submitted_order,
  models.enrollment_transaction,
  models.saved_search,
  models.assigned_license,
  models.ica_license,
  models.access_transaction]

async function getAll(req, res) {
  let where = {}

  if (req.query.trainingLocationId) {
    where['trainingLocationId'] = req.query.trainingLocationId
  } else {
    // Include those without TLs
    relModels[0]['required'] = false
    // Get all EAUsers from migrated TLs only
    relModels[0]["where"] = {"subscription_enabled": true}
  }
  if (req.query.role) {
    where['role'] = req.query.role
  }
  if (!req.query.withDeleted) {
    where['deleted'] = false
  }

  const instances = await model.findAll({
    where: where,
    include: relModels,
    required: false
  });
  res.status(200).json(serialize(instances, model));
}

async function getById(req, res) {
  const id = getIdParam(req);
  const instance = await findOne(id, model, relModels)
  if (instance) {
    res.status(200).json(serializeSingleton(instance, model, id));
  } else {
    res.status(404).send('404 - Not found');
  }
}

async function create(req, res) {
  if (req.params.id) {
    res.status(400).send(`Bad request: ID should not be provided, since it is determined automatically by the database.`)
  } else {
    let instance = await createAndUpdate(req, model, relModels)
    res.status(201).json(serializeSingleton(instance, model, instance["dataValues"]["id"]))
  }
}

async function update(req, res) {
  let instance = await createAndUpdate(req, model, relModels)
  if (instance) {
    if (!instance.active) {
      await removeTokens(instance)
    }
    res.status(201).json(serializeSingleton(instance, model, instance["dataValues"]["id"]))
  } else {
    res.status(404).send('404 - Not found');
  }
}

async function remove(req, res) {
  const id = getIdParam(req);

  let instance = await findOne(id, model, relModels)
  instance.deleted = true;
  await instance.save();
  //await ea.destroy();
  res.status(200).end();
};

module.exports = {
  getAll,
  getById,
  create,
  update,
  remove
};
