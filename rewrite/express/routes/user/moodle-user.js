const { Op } = require('sequelize');
const { models } = require('../../../sequelize');
const { serialize, serializeSingleton, getIdParam, createAndUpdate, findOne } = require('../../helpers');

const model = models.moodle_user
const relModels = [
  models.training_location,
  models.program_classification,
  models.user_type,
  models.enrollment_transaction_course_user,
  models.assigned_license,
  models.filter_group,
  models.assigned_ebook,
  models.ebook_transaction,
  models.subscription_user,
  models.assigned_ica_license,
  models.access_token_user
]

async function getAll(req, res) {
  let where = {}

  if (req.query.showRemoved) {
  } else {
    where['removed'] = false
  }

  if (req.query.trainingLocationId) {
    where['trainingLocationId'] = req.query.trainingLocationId
  }

  if (req.query.lmsUserId) {
    where['lmsUserId'] = req.query.lmsUserId
  }

  // Removed?
  /*if (req.query.dob) {
    where['dob'] = new Date(req.query.dob)
  }*/

  if (req.query.lastName) {
    where['lastName'] = req.query.lastName
  }

  if (req.query.email) {
    where['email'] = req.query.email
  }

  if (req.query.firstInitial) {
    where['firstName'] = {}
    where['firstName'][Op.like] = req.query.firstInitial + '%'
  }

  if (req.query.lmsIds) {
    let ids = req.query.lmsIds.split(',')
    where["id"] = ids
  }

  if (req.query.noAccessToken && req.query.trainingLocationId) {
    let now = new Date()
    let atuWhere = {}

    atuWhere[Op.and] = [
      {
        trainingLocationId: req.query.trainingLocationId
      },
      {
        [Op.or]: [
          {
            dateEnded: null
          },
          {
            dateEnded: {
              [Op.gt]: now
            }
          }
        ]
      }
    ]

    const atus = await relModels[10].findAll({attributes: ['moodle_user_id'], where: atuWhere})
    let userIds = []
    atus.forEach(function(item) {
      userIds.push(item["dataValues"]["moodle_user_id"].toString())
    });

    if (userIds.length > 0) {
      where["id"] = {}
      where["id"][Op.notIn] = userIds
    }
  }

  if (req.query.hasNoAccess && req.query.trainingLocationId) {
    let now = new Date()
    let suWhere = {}
    let atuWhere = {}

    suWhere[Op.and] = [
      {
        trainingLocationId: req.query.trainingLocationId
      },
      {
        [Op.or]: [
          {
            endedAt: null
          },
          {
            endedAt: {
              [Op.gt]: now
            }
          }
        ]
      }
    ]

    atuWhere[Op.and] = [
      {
        trainingLocationId: req.query.trainingLocationId
      },
      {
        [Op.or]: [
          {
            dateEnded: null
          },
          {
            dateEnded: {
              [Op.gt]: now
            }
          }
        ]
      }
    ]

    const sus = await relModels[8].findAll({attributes: ['moodle_user_id'], where: suWhere})
    const atus = await relModels[10].findAll({attributes: ['moodle_user_id'], where: atuWhere})
    let userIds = []
    sus.forEach(function(item) {
      userIds.push(item["dataValues"]["moodle_user_id"])
    });
    atus.forEach(function(item) {
      console.log(item)
      userIds.push(item["dataValues"]["moodle_user_id"])
    });

    if (userIds.length > 0) {
      where["id"] = {}
      where["id"][Op.notIn] = userIds
    }
  }

  const instances = await model.findAll({
    where: where,
    include: relModels
  });
  res.status(200).json(serialize(instances, model));
}

async function getById(req, res) {
  const id = getIdParam(req);
  const instances = await findOne(id, model, relModels)
  if (instances) {
    res.status(200).json(serializeSingleton(instances, model, id));
  } else {
    res.status(404).send('404 - Not found');
  }
}

async function create(req, res) {
  if (req.params.id) {
    res.status(400).send(`Bad request: ID should not be provided, since it is determined automatically by the database.`)
  } else {
    let instance = await createAndUpdate(req, model, relModels)
    res.status(201).json(serializeSingleton(instance, model, instance["dataValues"]["id"]))
  }
}

async function update(req, res) {
  let instance = await createAndUpdate(req, model, relModels)
  if (instance) {
    res.status(201).json(serializeSingleton(instance, model, instance["dataValues"]["id"]))
  } else {
    res.status(404).send('404 - Not found');
  }
}

async function remove(req, res) {
  const id = getIdParam(req);

  let instance = await findOne(id, model, relModels)
  instance.deleted = true
  await instance.save()

  // Soft-delete so don't remove these
  /*let rels = await instance.getEnrollment_transaction_course_users()
  rels.forEach(rel => instance.removeEnrollment_transaction_course_user(rel));

  rels = await instance.getAssigned_licenses()
  rels.forEach(rel => instance.removeAssigned_license(rel));

  rels = await instance.getFilter_groups()
  rels.forEach(rel => instance.removeFilter_group(rel));

  rels = await instance.getAssigned_ebooks()
  rels.forEach(rel => instance.removeAssigned_ebook(rel));

  rels = await instance.getEbook_transactions()
  rels.forEach(rel => instance.removeEbook_transaction(rel));

  rels = await instance.getSubscription_users()
  rels.forEach(rel => instance.removeSubscription_user(rel));

  rels = await instance.getAssigned_ica_licenses()
  rels.forEach(rel => instance.removeAssigned_ica_license(rel));

  rels = await instance.getAccess_token_users()
  rels.forEach(rel => instance.removeAccess_token_user(rel));

  await instance.destroy();*/
  res.status(200).end();
};

module.exports = {
  getAll,
  getById,
  create,
  update,
  remove
};
