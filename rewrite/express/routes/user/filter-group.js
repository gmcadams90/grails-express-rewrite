const { models } = require('../../../sequelize');
const { serialize, serializeSingleton, getIdParam, createAndUpdate, findOne } = require('../../helpers');
const { eaUserService } = require('../../services/user/ea-user-service')

const model = models.filter_group
const relModels = [{model: models.training_location}, models.moodle_user, models.access_request]

async function getAll(req, res) {
  let where = {}

  if (req.query.trainingLocationId) {
    where["trainingLocationId"] = req.query.trainingLocationId
  } else if (req.query.jatc) {
    relModels[0]["where"] = {"jatc": req.query.jatc}
  } else {
    let user = eaUserService.getCurrentUser(req.headers.authorization)
    if (!user) {
      throw new Error("No authenticated user")
    } else if (user.role == 'administrator') {
      throw new Error("No trainingLocationId specified")
    } else {
      where["trainingLocationId"] = user.trainingLocationId
    }
  }

  if (req.query.name) {
    where["name"] = req.query.name
  }

  const instances = await model.findAll({
    where: where,
    include: relModels
  });
  res.status(200).json(serialize(instances, model));
}

async function getById(req, res) {
  const id = getIdParam(req);
  const instances = await findOne(id, model, relModels)
  if (instances) {
    res.status(200).json(serializeSingleton(instance, model, id));
  } else {
    res.status(404).send('404 - Not found');
  }
}

async function create(req, res) {
  if (req.params.id) {
    res.status(400).send(`Bad request: ID should not be provided, since it is determined automatically by the database.`)
  } else {
    let instance = await createAndUpdate(req, model, relModels)
    res.status(201).json(serializeSingleton(instance, model, instance["dataValues"]["id"]))
  }
}

// TODO: May need to add additional deserialize logic here from incoming body
async function update(req, res) {
  let instance = await createAndUpdate(req, model, relModels)
  if (instance) {
    res.status(201).json(serializeSingleton(instance, model, instance["dataValues"]["id"]))
  } else {
    res.status(404).send('404 - Not found');
  }
}

async function remove(req, res) {
  const id = getIdParam(req);

  let instance = await findOne(id, model, relModels)
  let rels = await instance.getPermission_roles()
  rels.forEach(rel => instance.removePermission_role(rel));
  rels = await instance.getAccess_requests()
  rels.forEach(rel => instance.removeAccess_request(rel));
  rels = await instance.getMoodle_users()
  rels.forEach(rel => instance.removeMoodle_user(rel));
  await instance.destroy();
  res.status(200).end();
};

module.exports = {
  getAll,
  getById,
  create,
  update,
  remove
};
