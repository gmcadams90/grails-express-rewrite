const { models } = require('../../../sequelize');
const { serialize, serializeSingleton, getIdParam, createAndUpdate, findOne } = require('../../helpers');

const model = models.student_type
const relModels = [models.program_classification]

async function getAll(req, res) {
  const instances = await model.findAll({
    include: relModels
  });
  res.status(200).json(serialize(instances, model));
}

async function getById(req, res) {
  const id = getIdParam(req);
  const instances = await findOne(id, model, relModels)
  if (instances) {
    res.status(200).json(serializeSingleton(instance, model, id));
  } else {
    res.status(404).send('404 - Not found');
  }
}

async function create(req, res) {
  if (req.params.id) {
    res.status(400).send(`Bad request: ID should not be provided, since it is determined automatically by the database.`)
  } else {
    let instance = await createAndUpdate(req, model, relModels)
    res.status(201).json(serializeSingleton(instance, model, instance["dataValues"]["id"]))
  }
}

async function update(req, res) {
  let instance = await createAndUpdate(req, model, relModels)
  if (instance) {
    res.status(201).json(serializeSingleton(instance, model, instance["dataValues"]["id"]))
  } else {
    res.status(404).send('404 - Not found');
  }
}

async function remove(req, res) {
  const id = getIdParam(req);

  let instance = await findOne(id, model, relModels)
  let rels = await instance.getProgram_classifications()
  rels.forEach(rel => instance.removeProgram_classification(rel));
  await instance.destroy();
  res.status(200).end();
};

module.exports = {
  getAll,
  getById,
  create,
  update,
  remove
};
