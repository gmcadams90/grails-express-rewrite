const { models } = require('../../../sequelize');
const { serialize, serializeSingleton, getIdParam, createAndUpdate, findOne } = require('../../helpers');

const model = models.enrollment_transaction
const relModels = [models.training_location, models.ea_user, models.enrollment_transaction_course]

async function getAll(req, res) {
  let where = {}

  if (req.query.trainingLocationId) {
    where['trainingLocationId'] = req.query.trainingLocationId
  }

  const instances = await model.findAll({
    where: where,
    include: relModels
  });
  res.status(200).json(serialize(instances, model));
}

async function getById(req, res) {
  const id = getIdParam(req);
  const instance = await findOne(id, model, relModels)
  if (instance) {
    res.status(200).json(serializeSingleton(instance, model, id));
  } else {
    res.status(404).send('404 - Not found');
  }
}

async function create(req, res) {
  if (req.params.id) {
    res.status(400).send(`Bad request: ID should not be provided, since it is determined automatically by the database.`)
  } else {
    let instance = await createAndUpdate(req, model, relModels)
    res.status(201).json(serializeSingleton(instance, model, instance["dataValues"]["id"]))
  }
}

async function update(req, res) {
  let instance = await createAndUpdate(req, model, relModels)
  if (instance) {
    res.status(201).json(serializeSingleton(instance, model, instance["dataValues"]["id"]))
  } else {
    res.status(404).send('404 - Not found');
  }
}

async function remove(req, res) {
  const id = getIdParam(req);
  let et = await findOne(id, model, relModels)
  let etcs = await cs.getEnrollment_transaction_course()
  etcs.forEach(etc => et.removePermission(etc));
  await et.destroy();
  res.status(200).end();
}

module.exports = {
  getAll,
  getById,
  create,
  update,
  remove
};
