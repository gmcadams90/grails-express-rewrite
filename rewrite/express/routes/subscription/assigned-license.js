const { Op, Sequelize } = require('sequelize');
const { models } = require('../../../sequelize');
const { serialize, serializeSingleton, getIdParam, createAndUpdate, findOne, handleMissingParameter } = require('../../helpers');

const model = models.assigned_license
const relModels = [models.ea_user, models.moodle_user, models.training_location, models.course, models.enrollment_transaction_course_user]

async function getAll(req, res) {
  let where = {}

  where[Op.and] = []

  if (req.query.moodleUserId) {
    where[Op.and].push({moodleUserId: req.query.moodleUserId})
  }
  if (req.query.courseId) {
    where[Op.and].push({courseId: req.query.courseId})
  }
  if (req.query.trainingLocationId) {
    where[Op.and].push({trainingLocationId: req.query.trainingLocationId})
  }
  if (req.query.transactionId) {
    let enrollmentTransactionCourses = await models.enrollment_transaction_course.findAll({
      where: {enrollmentTransactionId: req.query.transactionId},
      attributes: [Sequelize.fn('DISTINCT', Sequelize.col('id')), 'id']
    })
    let enrollmentTransactionCoursesIds = []
    enrollmentTransactionCourses.forEach(function(item) {
      enrollmentTransactionCoursesIds.push(item["dataValues"]["id"])
    })
    let enrollmentTransactionCourseUsers = await models.enrollment_transaction_course_user.findAll({
      where: {enrollmentTransactionCourseId: enrollmentTransactionCoursesIds},
      attributes: [Sequelize.fn('DISTINCT', Sequelize.col('id')), 'id']
    })
    let enrollmentTransactionCourseUsersIds = []
    enrollmentTransactionCourseUsers.forEach(function(item) {
      enrollmentTransactionCourseUsersIds.push(item["dataValues"]["id"])
    })
    where[Op.and].push({enrollmentTransactionCourseUserId: enrollmentTransactionCourseUsersIds})
  }

  if (!req.query.includeInactive) {
    const now = new Date().toLocaleString('en-US', {
      timeZone: 'GMT'
    })
    where["dateExpired"] = {}
    where["dateExpired"][Op.or] = {
      [Op.gt]: now,
      [Op.eq]: null
    }
  }

  const instances = await model.findAll({
    where: where,
    include: relModels
  });
  res.status(200).json(serialize(instances, model));
}

async function getById(req, res) {
  const id = getIdParam(req);
  const instances = await findOne(id, model, relModels)
  if (instances) {
    res.status(200).json(serializeSingleton(instance, model, id));
  } else {
    res.status(404).send('404 - Not found');
  }
}

async function create(req, res) {
  if (req.params.id) {
    res.status(400).send(`Bad request: ID should not be provided, since it is determined automatically by the database.`)
  } else {
    let instance = await createAndUpdate(req, model, relModels)
    res.status(201).json(serializeSingleton(instance, model, instance["dataValues"]["id"]))
  }
}

async function update(req, res) {
  let instance = await createAndUpdate(req, model, relModels)
  if (instance) {
    res.status(201).json(serializeSingleton(instance, model, instance["dataValues"]["id"]))
  } else {
    res.status(404).send('404 - Not found');
  }
}

async function remove(req, res) {
  const id = getIdParam(req);

  let instance = await findOne(id, model, relModels)
  await instance.destroy();
  res.status(200).end();
}

module.exports = {
  getAll,
  getById,
  create,
  update,
  remove
};
