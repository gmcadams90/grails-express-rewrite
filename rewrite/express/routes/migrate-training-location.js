const { models } = require('../../sequelize');
const { handleMissingParameter } = require("../helpers");
const { eaUserService } = require('../../services/user/ea-user-service')

// migrateTrainingLocation
async function create(req, res) {
  // TODO: eaUserService
  let eaUser = eaUserService.getCurrentUser(req.headers.authorization)
  let propsJson = req.JSON
  let reqparams = {}

  if (!propsJson.data) {
    res.status(422).json(handleMissingParameter("data"))
  }

  propsJson = propsJson.data

  if (!propsJson.tl_lms_id) {
    res.status(422).json(handleMissingParameter("tl_lms_id"))
  }

  let trainingLocation = await model.findOne({
    where: {"lmsId": propsJson.tl_lms_id}
  })

  if (!trainingLocation) {
    res.status(422).json({"message": `Training Location ${propsJson.tl_lms_id} does not exist in Subs TMS`})
  } else if (trainingLocation.subscriptionEnabled) {
    res.status(422).json({"message": `Training Location ${trainingLocation.lmsId} has already been migrated`})
  }

  console.log("Starting migration for " + propsJson.tl_tms_id)

  // Store all newly created temp Assigned Licenses
  let assignedLicenses = []
  // Store all newly created Subscription Users
  let subscriptionUsers = []
  // Store all newly created Moodle Users
  let createdMoodleUsers = []
  // Store all newly created Courses
  let createdCourses = []

  // Store ALL moodleUsers to avoid repeated queries
  let moodleUsersById = {}
  // Store ALL courses to avoid repeated queries
  let coursesById = {}


}

module.exports = {
  create
};
