const { models } = require('../sequelize');
const UIDGenerator = require('uid-generator');
const bcrypt = require('bcrypt');
const moment = require('moment')

async function getAccessToken(req, res) {
  let resError = {}
  let grantType

  // Require grant_type in body
  // Supporting only 'password' and 'client_credentials' currently
  if (!req.body["grant_type"]) {
    resError["error"] = "Grant type missing"
    return res.status(400).json(resError)
  } else if (!(req.body["grant_type"] == 'password' || req.body["grant_type"] == 'client_credentials')) {
    resError["error"] = "Supported grant types: 'password' and 'client_credentials'"
    return res.status(400).json(resError)
  } else {
    grantType = req.body["grant_type"]
  }

  // Verify client_id/secret
  let header = req.headers.authorization || ''; // get auth header
  let token = header.split(/\s+/).pop() || ''; // encoded part
  let auth = Buffer.from(token, 'base64').toString(); // convert from base64
  let parts = auth.split(/:/); // split on colon
  let id = parts.shift(); // username is first
  let secret = parts.join(':'); // everything else is the password

  let oauth_client = await models.oauth_client_details.findOne({where: {client_id: id, client_secret: secret}})

  // If Basic auth failed
  if (!oauth_client) {
    resError["error"] = "Bad client credentials"
    return res.status(400).json(resError)
  }

  if (grantType == "password") {
    return await grantUserAccessToken(req, res)
  } else {
    return await grantClientAccessToken(req, res, oauth_client)
  }
}

async function grantClientAccessToken(req, res, client) {
  let resError = {}

  // Client guaranteed to exist here; check creds
  if (req.body["client_secret"] != client.client_secret) {
    resError["error"] = "Unauthorized"
    resError["message"] = "Bad credentials"
    return res.status(400).json(resError)
  } else {
    return res.status(200).json(await checkForExistingToken(models.oauth_client_tokens, "client_id", client.client_id))
  }
}

async function grantUserAccessToken(req, res) {
  let resError = {}

  // Verify email/password exists for an EAUser
  let email = req.body["username"]
  let user = await models.ea_user.findOne({where: {email: email}})

  // If user exists
  if (user) {
    // Compare encrypted stored password with request password
    let matches = await bcrypt.compare(req.body["password"], user.password)
    if (!matches) {
      resError["error"] = "Unauthorized"
      resError["message"] = "Bad credentials"
      return res.status(400).json(resError)
    }
    return res.status(200).json(await checkForExistingToken(models.oauth_user_tokens, "eaUserId", user.id))
  } else {
    resError["error"] = "Unauthorized"
    resError["message"] = "Unknown user"
    return res.status(400).json(resError)
  }
}

async function checkForExistingToken(tokenModel, tokenKey, tokenValue) {
  let resError = {}
  let now = moment()
  let nextDay = moment(now).add(1, 'days')
  const newUid = new UIDGenerator(128, UIDGenerator.BASE16)

  // Check for existing token
  let existingToken = await tokenModel.findOne({where: {[tokenKey]: tokenValue}})
  // If token exists for a user/client and has not expired yet
  if (existingToken && now < existingToken.expiration) {
    resError['access_token'] = existingToken.token
    resError['expiration'] = existingToken.expiration
  } else if (existingToken) {
    // If token exists but is expired, get new key and update expiration date
    existingToken = await existingToken.update({
      token: await newUid.generate(),
      expiration: nextDay
    })
    resError['access_token'] = existingToken.token
    resError['expiration'] = existingToken.expiration
  } else {
    // Else, no entry for this user yet, so create one
    let newToken = await tokenModel.create({
      token: await newUid.generate(),
      expiration: nextDay,
      [tokenKey]: tokenValue
    })

    resError['access_token'] = newToken.token
    resError['expiration'] = newToken.expiration
  }
  resError['token_type'] = "bearer"

  return resError
}

// Use this to verify access to any controller
// Returns the JSON response; error if map contains 'error'
async function verifyAccess(req, res, requiresAuth, allowed_roles=null) {
  let resError = {}

  // Permits all
  if (!requiresAuth) {
    return resError
  }

  // For user/password tokens
  let header = req.headers.authorization; // get auth header
  if (!header) {
    resError['error'] = "unauthorized"
    resError['error_description'] = "Full authentication is required to access this resource"
  }

  let now = moment()
  let token = header.split(/\s+/).pop() || ''; // encoded part
  let userToken = await models.oauth_user_tokens.findOne({where: {token: token}})
  // If token exists and is tied to a user
  if (userToken) {
    let expiration = userToken.expiration
    if (now > expiration) {
      resError['error'] = "expired token"
      resError['error_description'] = "Token has expired"
    } else {
      let user = await models.ea_user.findOne({where: {id: userToken.eaUserId}})

      // If roles are given, verify user has an allowed role
      if (allowed_roles) {
        if (!allowed_roles.includes(user.role)) {
          resError['error'] = "bad role"
          resError['error_description'] = "User does not have the proper role to access this resource"
        }
      } else if (!user) { // User doesn't exist anymore
        resError['error'] = "invalid user"
        resError['error_description'] = "Invalid user for access token: " + token
      }
    }
  } else {
    // Check for client token if no user is found
    let clientToken = await models.oauth_client_tokens.findOne({where: {token: token}})

    if (clientToken) {
      let expiration = clientToken.expiration
      if (now > expiration) {
        resError['error'] = "expired token"
        resError['error_description'] = "Token has expired"
      } else {
        let client = await models.oauth_client_details.findOne({where: {client_id: clientToken.client_id}})
        if (!client) {
          resError['error'] = "invalid token"
          resError['error_description'] = "Invalid client for access token: " + token
        }
      }
    } else { // Token isn't found anywhere
      resError['error'] = "bad token"
      resError['error_description'] = "Token not found"
    }
  }

  return resError
}

// Get tokens for a given user
async function getTokens(user) {
  let tokenList = []
  let tokens = await models.oauth_client_tokens.findAll({where: {eaUserId: user.id}})
  tokens.forEach(function(token) {
    tokenList.push(token["dataValues"]["token"])
  });

  return tokenList
}

// Remove tokens for a given user
async function removeTokens(user) {
  await models.oauth_client_tokens.destroy({where: {eaUserId: user.id}})
}

module.exports = { getAccessToken, verifyAccess, getTokens, removeTokens };
