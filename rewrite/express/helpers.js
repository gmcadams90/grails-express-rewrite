const { nodemailer } = require("nodemailer");
const { hbs } = require('nodemailer-express-handlebars');
const config = require('./config');

const mail_host = config.mail.host
const mail_port = config.mail.port
const mail_username = config.mail.username
const mail_password = config.mail.password
const mail_from = config.mail.default.from

async function sendMail(to, subject, template, context) {
  let transporter = nodemailer.createTransport({
    host: mail_host,
    port: mail_port,
    secure: false, // true for 465, false for other ports
    auth: {
      user: mail_username, // generated ethereal user
      pass: mail_password // generated ethereal password
    }
  });

  let options = {
    viewEngine : {
      extname: '.hbs', // handlebars extension
      layoutsDir: 'views', // location of handlebars templates
      defaultLayout: '', // name of main template
      partialsDir: 'views/partials', // location of your subtemplates aka. header, footer etc
    },
    viewPath: 'views',
    extName: '.hbs'
  };
  transporter.use('compile', hbs(options));
  let mail = {
    from: mail_from,
    to: to,
    subject: subject,
    template: template,
    context: context
  }
  await transporter.sendMail(mail);
}

// A helper function to assert the request ID param is valid
// and convert it to a number (since it comes as a string by default)
function getIdParam(req) {
	const id = req.params.id;
	if (/^\d+$/.test(id)) {
		return Number.parseInt(id, 10);
	}
	throw new TypeError(`Invalid ':id' param: "${id}"`);
}

function toCamelCase(input, separator, suffix=null) {
	let split = input.split(separator);
	let out = "";

	if (split.length > 1) {
		out = split[0] + capitalizeFirstLetter(split[1]);
    split.shift()
    // More than 2 parts
		while (split.length > 1) {
		  split.shift()
		  out += capitalizeFirstLetter(split[0])
    }
	} else {
		out = split[0]
	}

  if (suffix) {
    out += suffix
  }

	return out
}

function fromCamelCase(input, separator) {
  let split = input.split(/(?<=[a-z])(?=[A-Z])/);
  let out = "";

  if (split.length > 1) {
    out += split[0].toLowerCase() + separator + split[1].toLowerCase()
    split.shift()
    split.shift()
    while (split.length > 1) {
      out += separator + split[0].toLowerCase()
      split.shift()
    }
    if (split.length > 0) {
      out += separator + split[0].toLowerCase()
    }
  } else {
    out = split[0]
  }

  return out
}

// Transform into JSON spec API (copy Grails)
function serialize(result, model) {
	let type = toCamelCase(model.name, "_")
	let res = {"data": []}

	// Get attributes and relationships from model
  let attrs = model.rawAttributes
  let rels = model.associations

	result.forEach(function(item) {
		let row = {"type": type, "id": item['id'].toString(), "attributes": {}}
		row["attributes"] = {}
		for (let attr of Object.keys(attrs)) {
      row["attributes"][attr] = item["dataValues"][attr]
    }
		row["relationships"] = {}
		for (let rel of Object.keys(rels)) {
		  let relCamel = toCamelCase(rel, '_')
			// If hasMany
			if (Array.isArray(item[rel])) {
				row["relationships"][relCamel] = {"data":[]}
				item[rel].forEach(function (pr) {
					// Get rid of 's' in type
					row["relationships"][relCamel]["data"].push({"type": relCamel.slice(0, -1), "id": pr["id"].toString()})
				});
			} else { // Else, belongsTo
				row["relationships"][relCamel] = {"data":{}}

        if (item[rel]) {
          row["relationships"][relCamel]["data"] = {"type": relCamel, "id": item[rel]["dataValues"]["id"].toString()}
          row["relationships"][relCamel]["links"] = {"self": "/" + relCamel + "/show/" + item[rel]["dataValues"]["id"]}
        }
			}
		};
		res["data"].push(row)
	});

	res["links"] = {"self": "/" + pluralize(fromCamelCase(type,"-"))}

	return res
}

function serializeSingleton(result, model, id) {
  let output = serialize([result], model)
  let singleton = {"data": output["data"][0]}
  output["links"]["self"] += "/show/" + id
  singleton["links"] = output["links"]

  return singleton
}

function capitalizeFirstLetter(string) {
	return string.charAt(0).toUpperCase() + string.slice(1);
}

// Normalize attributes and get relationship objects
async function deserialize(attrsIn, relsIn, model, relModels) {
  let res = {"relationships": {}, "attributes": {}}

  // Get column meta info
  let meta = await model.describe()

  // Get each belongsTo relationship
  for (const [key, value] of Object.entries(meta)) {
    if (key.endsWith("_id")) { // Ex. training_location_id
      let camel = toCamelCase(key, "_") // Ex. trainingLocationId
      let camelNoId = camel.split("Id")[0] // Ex. trainingLocation
      if (relsIn[camelNoId]) {
        attrsIn[camel] = relsIn[camelNoId]["data"]["id"]
        relsIn[camelNoId] = null
      }
    }
  }

  // Get each hasMany relationship
  for (const [key, value] of Object.entries(relsIn)) {
    let ids = []
    if (value == null) {
      continue
    }

    if (Array.isArray(value["data"])) {
      value["data"].forEach(item => ids.push(item["id"]))
    } else { // Handle possible singleton case
      ids.push(value["data"]["id"])
    }

    let relModelName = singularize(fromCamelCase(key, '_'))
    // TODO: Make this more efficient
    for (const relModel of relModels) {
      if (relModel.name === relModelName) {
        res["relationships"][relModelName] = await relModel.findAll({where: {id: ids}})
      }
    }
  }

  // Set to updated attributes
  res["attributes"] = attrsIn

  return res
}

async function updateGeneric(id, model, attrs, rels) {
  // Create using attributes
  let instance = await model.findOne({
    where: {id: id}
  });
  if (!instance) {
    return null;
  }

  await model.update(attrs, {
    where: {
      id: id
    }
  });

  // TODO: Maybe optimize by not having to remove/add each
  for (const [rel, relInstances] of Object.entries(rels)) {
    let oldRels = await eval("instance.get"+capitalizeFirstLetter(rel)+"s()")
    oldRels.forEach(oldRel => eval("instance.remove"+capitalizeFirstLetter(rel)+"(oldRel)"))
    for (const relInstance of relInstances) {
      await eval("instance.add"+capitalizeFirstLetter(rel)+"(relInstance)")
    }
  }

  return instance
}

async function createGeneric(model, attrs, rels) {
  let instance = await model.create(attrs)

  for (const [rel, relInstances] of Object.entries(rels)) {
    for (const relInstance of relInstances) {
      await eval("instance.add"+capitalizeFirstLetter(rel)+"(relInstance)")
    }
  }

  return instance["dataValues"]["id"]
}

async function createAndUpdate(req, model, relModels) {
  let attrsIn = req.body["data"]["attributes"];
  let relsIn =  req.body["data"]["relationships"];
  let id = null
  let instance = null

  // Normalize relationships and get relationship models
  let attrsRels = await deserialize(attrsIn, relsIn, model, relModels)

  // Create using attributes
  if (req.params.id) {
    id = getIdParam(req);
    instance = await updateGeneric(id, model, attrsRels["attributes"], attrsRels["relationships"])
    if (!instance) {
      return null
    }
  } else {
    id = await createGeneric(model, attrsRels["attributes"], attrsRels["relationships"])
  }

  // Query so we can get the fresh object
  instance = await findOne(id, model, relModels)

  return instance
}

async function findOne(id, model, relModels) {
  const instance = await model.findOne({
    where: {id: id},
    include: relModels
  });

  return instance
}

function formatDate(date, char) {
  let d = new Date(date),
    month = '' + (d.getMonth() + 1),
    day = '' + d.getDate(),
    year = d.getFullYear();

  if (month.length < 2)
    month = '0' + month;
  if (day.length < 2)
    day = '0' + day;

  return [year, month, day].join(char);
}

function handleMissingParameter(propertyName) {
  let res = {"message": "Property(ies) [" + propertyName + "] missing from the request"}
  return res
}

function pluralize(input) {
  if (input.endsWith('ch') || input.endsWith('sh') || input.endsWith('x') || input.endsWith('z')) {
    return input + 'es'
  } else {
    return input + 's'
  }
}

function singularize(input) {
  if (input.endsWith('ch') || input.endsWith('sh') || input.endsWith('x') || input.endsWith('z')) {
    // Ex. savedSearches -> savedSearch
    return input.slice(0, -2)
  } else {
    // Ex. permissions -> permission
    return input.slice(0, -1)
  }
}

module.exports = { getIdParam, serialize, serializeSingleton, createAndUpdate, findOne, formatDate, handleMissingParameter, sendMail };
