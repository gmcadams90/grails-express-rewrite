function applyExtraSetup(sequelize) {
	const {
	  permission,
    permission_role,
    training_location,
    ea_user,
    subscription_training_location_tier,
    cart_item,
    course,
    course_group,
    course_session,
    custom_course,
    prerequisite,
    assigned_ebook,
    ebook_transaction,
    ebook_transaction_group,
    user_type,
    import_error,
    readynamic_create_account_access_token,
    completion_transaction,
    access_log,
    program_classification,
    pending_enrollment,
    submitted_enrollment,
    submitted_order,
    submitted_order_item,
    enrollment_transaction,
    moodle_user,
    subscription_feature,
    enrollment_transaction_course_user,
    enrollment_transaction_course,
    training_location_group,
    access_request,
    filter_group,
    subscription,
    subscription_price,
    course_completion,
    subscription_user,
    access_transaction,
    saved_search,
    import_run,
    student_type,
    access_token,
    access_window,
    access_token_user,
    training_location_access_window,
    assigned_ica_course_group,
    assigned_ica_license,
    ica_course_group,
    ica_license,
    assigned_license,
    oauth_user_tokens,
    oauth_client_details,
    oauth_client_tokens
	} = sequelize.models;

	permission.belongsToMany(permission_role, {
		through: "permission_role_permissions",
		foreignKey: "permission_id"
	});

	permission_role.belongsToMany(permission, {
		through: "permission_role_permissions",
		foreignKey: "permission_role_id"
	});
	permission_role.belongsTo(training_location)
	permission_role.hasMany(ea_user)

	training_location.hasMany(permission_role)
  training_location.hasMany(ea_user, {foreignKey: {allowNull: true}})
  training_location.belongsTo(subscription_training_location_tier)
  training_location.hasMany(cart_item)
  training_location.belongsToMany(course, {
    through: "training_location_courses",
    foreignKey: "training_location_id"
  })
  training_location.hasMany(course_group)
  training_location.hasMany(course_session)
  training_location.hasMany(custom_course)
  training_location.hasMany(assigned_ebook)
  training_location.hasMany(ebook_transaction)
  training_location.hasMany(ebook_transaction_group)
  training_location.belongsToMany(program_classification, {
    through: 'training_location_program_classification',
    foreignKey: 'training_location_program_classifications_id'
  })
  training_location.hasMany(pending_enrollment)
  training_location.hasMany(submitted_enrollment)
  training_location.hasMany(submitted_order)
  training_location.hasMany(completion_transaction)
  training_location.hasMany(enrollment_transaction)
  training_location.hasMany(access_request)
  training_location.hasMany(filter_group)
  /*training_location.belongsToMany(test_generator, {
    through: 'training_location_test_generators',
    foreignKey: 'training_location_id'
  })*/
  training_location.hasMany(moodle_user)
  training_location.hasMany(enrollment_transaction)
  training_location.hasMany(enrollment_transaction_course)
  training_location.hasMany(enrollment_transaction_course_user)
  training_location.hasMany(assigned_license)
  training_location.hasMany(saved_search)
  training_location.hasMany(course_completion)
  training_location.belongsToMany(training_location_group, {
    through: 'training_location_training_location_groups',
    foreignKey: 'training_location_id'
  })
  training_location.hasMany(subscription)
  training_location.hasMany(subscription_user)
  training_location.hasMany(subscription_feature)
  training_location.hasMany(ica_license)
  training_location.hasMany(training_location_access_window)
  training_location.hasMany(access_token)
  training_location.belongsToMany(access_window, {
    through: 'training_location_access_window',
    foreignKey: 'training_location_id'
  })

	ea_user.belongsTo(permission_role)
  ea_user.belongsTo(training_location, {foreignKey: {allowNull: true}})
  ea_user.hasMany(cart_item)
  ea_user.hasMany(custom_course)
  ea_user.hasMany(ebook_transaction)
  ea_user.hasMany(ebook_transaction_group)
  ea_user.belongsTo(user_type)
  ea_user.hasMany(pending_enrollment)
  ea_user.hasMany(submitted_order)
  ea_user.hasMany(saved_search)
  ea_user.hasMany(enrollment_transaction)
  ea_user.hasMany(assigned_license)
  ea_user.hasMany(ica_license)
  ea_user.hasMany(access_transaction)
  ea_user.hasMany(oauth_user_tokens)

  subscription_training_location_tier.hasMany(training_location)
  subscription_training_location_tier.hasMany(subscription_feature)

  cart_item.belongsTo(training_location)
  cart_item.belongsTo(ea_user)
  cart_item.belongsTo(course_session)
  cart_item.belongsTo(course)
  cart_item.hasMany(pending_enrollment)

  course.belongsToMany(training_location, {
    through: "training_location_courses",
    foreignKey: "course_id"
  })
  course.belongsToMany(course_group, {
    through: "course_group_courses",
    foreignKey: "course_id"
  })
  course.hasMany(course_session)
  //course.hasMany(prerequisite)
  course.hasMany(enrollment_transaction_course)
  course.belongsToMany(access_request, {
    through: "access_request_courses",
    foreignKey: 'course_id'
  })
  course.hasMany(assigned_license)
  course.belongsToMany(ica_course_group, {
    through: "course_ica_course_groups",
    foreignKey: "course_id"
  })

  course_group.belongsToMany(course, {
    through: "course_group_courses",
    foreignKey: "course_group_id"
  })
  course_group.belongsTo(training_location)
  course_group.hasMany(course_session)

  course_session.belongsTo(course)
  course_session.belongsTo(course_group)
  course_session.belongsTo(course_session, {as: 'parent_session'})
  //course_session.hasMany(course_session)
  course_session.belongsTo(training_location)
  course_session.hasOne(cart_item)
  course_session.hasMany(pending_enrollment)
  course_session.hasMany(submitted_enrollment)
  course_session.hasMany(submitted_order_item)
  course_session.hasMany(enrollment_transaction_course)

  custom_course.belongsTo(training_location)
  custom_course.belongsTo(ea_user)

  prerequisite.belongsTo(course, {'as': 'courses'})
  prerequisite.belongsTo(course, {'as': 'required_course'})

  assigned_ebook.belongsTo(training_location)
  assigned_ebook.belongsTo(ea_user)
  assigned_ebook.hasMany(ebook_transaction)
  assigned_ebook.belongsTo(moodle_user)

  ebook_transaction.belongsTo(training_location)
  ebook_transaction.belongsTo(ea_user)
  ebook_transaction.belongsTo(assigned_ebook)
  ebook_transaction.belongsTo(ebook_transaction_group)
  ebook_transaction.belongsTo(moodle_user)

  ebook_transaction_group.belongsTo(training_location)
  ebook_transaction_group.belongsTo(ea_user)
  ebook_transaction_group.hasMany(ebook_transaction)

  user_type.hasMany(ea_user)
  user_type.hasMany(moodle_user)

  import_error.belongsTo(import_run)

  readynamic_create_account_access_token.belongsTo(training_location)
  readynamic_create_account_access_token.belongsTo(moodle_user)

  completion_transaction.belongsTo(ea_user)
  completion_transaction.belongsTo(training_location)
  completion_transaction.hasMany(course_completion)

  access_log.belongsTo(ea_user, {'as': 'user'})
  access_log.belongsTo(permission, {'as': 'permission'})

  program_classification.belongsToMany(student_type, {
    through: 'program_classification_student_types',
    foreignKey: 'program_classification_id'
  })
  program_classification.belongsToMany(training_location, {
    through: 'training_location_program_classification',
    foreignKey: 'program_classification_id'
  })
  program_classification.hasMany(access_request)
  program_classification.hasMany(moodle_user)

  pending_enrollment.belongsTo(training_location)
  pending_enrollment.belongsTo(cart_item)
  pending_enrollment.belongsTo(ea_user)
  pending_enrollment.belongsTo(course_session)

  submitted_enrollment.belongsTo(course_session)
  submitted_enrollment.belongsTo(training_location)
  submitted_enrollment.belongsTo(submitted_order_item)

  submitted_order.belongsTo(training_location)
  submitted_order.belongsTo(ea_user)
  submitted_order.hasMany(submitted_order_item)

  submitted_order_item.belongsTo(submitted_order)
  submitted_order_item.belongsTo(course_session)
  submitted_order_item.hasMany(submitted_enrollment)

  enrollment_transaction.hasMany(enrollment_transaction_course)
  enrollment_transaction.belongsTo(training_location)
  enrollment_transaction.belongsTo(ea_user)

  moodle_user.belongsTo(training_location)
  moodle_user.belongsTo(program_classification)
  moodle_user.belongsTo(user_type)
  moodle_user.hasMany(enrollment_transaction_course_user)
  moodle_user.hasMany(assigned_license)
  moodle_user.belongsToMany(filter_group, {
    through: 'moodle_user_filter_groups',
    foreignKey: 'moodle_user_id'
  })
  moodle_user.hasMany(assigned_ebook)
  moodle_user.hasMany(ebook_transaction)
  moodle_user.hasMany(subscription_user)
  moodle_user.hasMany(assigned_ica_license)
  moodle_user.hasMany(access_token_user)

  subscription_feature.belongsTo(training_location)
  subscription_feature.belongsTo(subscription_training_location_tier)

  enrollment_transaction_course_user.belongsTo(training_location)
  enrollment_transaction_course_user.belongsTo(enrollment_transaction_course)
  enrollment_transaction_course_user.belongsTo(moodle_user)
  enrollment_transaction_course_user.hasOne(assigned_license)

  enrollment_transaction_course.belongsTo(training_location)
  enrollment_transaction_course.belongsTo(enrollment_transaction)
  enrollment_transaction_course.belongsTo(course)
  enrollment_transaction_course.belongsTo(course_session)
  enrollment_transaction_course.hasMany(enrollment_transaction_course_user)

  training_location_group.belongsToMany(training_location, {
    through: 'training_location_training_location_groups',
    foreignKey: 'training_location_group_id'
  })

  access_request.belongsTo(training_location)
  access_request.belongsTo(program_classification, {foreignKey: {allowNull: true}})
  access_request.belongsTo(student_type, {foreignKey: {allowNull: true}})
  access_request.belongsToMany(filter_group, {
    through: 'access_request_filter_groups',
    foreignKey: 'access_request_id'
  })
  access_request.belongsToMany(course, {
    through: "access_request_courses",
    foreignKey: 'access_request_id'
  })

  filter_group.belongsToMany(moodle_user, {
    through: 'moodle_user_filter_groups',
    foreignKey: 'filter_group_id'
  })
  filter_group.belongsToMany(access_request, {
    through: 'access_request_filter_groups',
    foreignKey: 'filter_group_id'
  })
  filter_group.belongsTo(training_location)

  subscription.hasMany(subscription_user)
  subscription.belongsTo(training_location)
  subscription.belongsTo(subscription_price)
  subscription.belongsTo(access_transaction, {as: "access_transaction"})
  subscription.belongsTo(access_transaction, {as: "cancel_transaction", foreignKey: {allowNull: true}})

  subscription_price.hasMany(subscription)

  course_completion.belongsTo(training_location)
  course_completion.belongsTo(ea_user)
  course_completion.belongsTo(completion_transaction)

  subscription_user.belongsTo(training_location)
  subscription_user.belongsTo(subscription, {foreignKey: {allowNull: true}})
  subscription_user.belongsTo(moodle_user)
  subscription_user.belongsTo(access_transaction, {as: "access_transaction"})
  subscription_user.belongsTo(access_transaction, {as: "cancel_transaction", foreignKey: {allowNull: true}})

  access_transaction.hasMany(subscription)
  access_transaction.hasMany(subscription_user)
  access_transaction.hasMany(access_token)
  access_transaction.hasMany(access_token_user)
  access_transaction.belongsTo(training_location)
  access_transaction.belongsTo(ea_user)

  saved_search.belongsTo(training_location)
  saved_search.belongsTo(ea_user)

  import_run.hasMany(import_error)
  import_run.belongsTo(training_location)
  import_run.belongsTo(ea_user)

  student_type.belongsToMany(program_classification, {
    through: "program_classification_student_types",
    foreignKey: "student_type_id"
  })
  student_type.hasMany(access_request)

  access_token.hasMany(access_token_user)
  access_token.belongsTo(training_location)
  access_token.belongsTo(training_location_access_window)
  access_token.belongsTo(access_transaction, {as: "access_transaction"})
  access_token.belongsTo(access_transaction, {as: "cancel_transaction", foreignKey: {allowNull: true}})

  access_window.hasMany(training_location_access_window)

  access_token_user.belongsTo(moodle_user)
  access_token_user.belongsTo(access_token)
  access_token_user.belongsTo(training_location)
  access_token_user.belongsTo(access_transaction, {as: "access_transaction"})
  access_token_user.belongsTo(access_transaction, {as: "cancel_transaction", foreignKey: {allowNull: true}})

  training_location_access_window.belongsTo(training_location)
  training_location_access_window.belongsTo(access_window)
  training_location_access_window.hasMany(access_token)

  assigned_ica_course_group.belongsTo(training_location)
  assigned_ica_course_group.belongsTo(moodle_user)
  assigned_ica_course_group.belongsTo(ica_course_group)
  assigned_ica_course_group.belongsTo(ea_user)

  assigned_ica_license.belongsTo(moodle_user)
  assigned_ica_license.belongsTo(ea_user)
  assigned_ica_license.belongsTo(ica_license)

  ica_course_group.belongsToMany(course, {
    through: 'course_ica_course_groups',
    foreignKey: "ica_course_group_id"
  })
  ica_course_group.hasMany(ica_license)

  ica_license.hasMany(assigned_ica_license)
  ica_license.belongsTo(ea_user)
  ica_license.belongsTo(training_location)
  ica_license.belongsTo(ica_course_group)

  assigned_license.belongsTo(ea_user)
  assigned_license.belongsTo(moodle_user)
  assigned_license.belongsTo(training_location)
  assigned_license.belongsTo(course)
  assigned_license.belongsTo(enrollment_transaction_course_user)

  oauth_user_tokens.belongsTo(ea_user)

  // TODO: Fix FK constraint fails
  //oauth_client_details.hasMany(oauth_client_tokens)
  //oauth_client_tokens.belongsTo(oauth_client_details, {as: "client_id"})
}

module.exports = { applyExtraSetup };
