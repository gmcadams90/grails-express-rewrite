const { DataTypes } = require('sequelize');

// We export a function that defines the model.
// This function will automatically receive as parameter the Sequelize connection object.
module.exports = (sequelize) => {
  sequelize.define('import_run', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    absoluteFilename: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: false
    },
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE,
      unique: false
    },
    filename: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: false
    },
    finishedAt: {
      allowNull: true,
      type: DataTypes.DATE,
      unique: false
    },
    rowsImported: {
      allowNull: true,
      type: DataTypes.INTEGER,
      unique: false
    },
    startedAt: {
      allowNull: true,
      type: DataTypes.DATE,
      unique: false
    },
    status: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: false,
      defaultValue: "waiting",
      validate: {
        isIn: [["waiting", "running", "failed", "succeeded", "errors"]]
      }
    },
    totalRows: {
      allowNull: true,
      type: DataTypes.INTEGER,
      unique: false
    },
    subscriptionId: {
      allowNull: true,
      type: DataTypes.BIGINT,
      unique: false
    },
    type: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: false,
      validate: {
        isIn: [["user", "qualification"]]
      }
    }
  });
};
