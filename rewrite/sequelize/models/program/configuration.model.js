const { DataTypes } = require('sequelize');

// We export a function that defines the model.
// This function will automatically receive as parameter the Sequelize connection object.
module.exports = (sequelize) => {
  sequelize.define('configuration', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    value: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    },
    key: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: false
    },
    type: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: false
    }
  });
};
