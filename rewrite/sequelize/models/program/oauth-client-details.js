const { DataTypes } = require('sequelize');

// We export a function that defines the model.
// This function will automatically receive as parameter the Sequelize connection object.
module.exports = (sequelize) => {
  sequelize.define('oauth_client_details', {
    client_id: {
      allowNull: false,
      autoIncrement: false,
      primaryKey: true,
      type: DataTypes.STRING
    },
    client_secret: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    },
    scope: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    },
    access_token_validity: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    }
  });
};
