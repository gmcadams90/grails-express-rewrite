const { DataTypes } = require('sequelize');
const uuid = require('uuid');

// We export a function that defines the model.
// This function will automatically receive as parameter the Sequelize connection object.
module.exports = (sequelize) => {
	sequelize.define('training_location', {
		id: {
			allowNull: false,
			autoIncrement: true,
			primaryKey: true,
			type: DataTypes.INTEGER
		},
		dateCreated: {
			allowNull: false,
			type: DataTypes.DATE
		},
		lastUpdated: {
			allowNull: false,
			type: DataTypes.DATE
		},
		name: {
			allowNull: false,
			type: DataTypes.STRING,
			validate: {
				max: 255
			}
		},
		jatc: {
			allowNull: false,
			type: DataTypes.STRING,
			validate: {
				max: 255
			}
		},
		hidden: {
			allowNull: false,
			type: DataTypes.BOOLEAN
		},
		billable: {
			allowNull: false,
			type: DataTypes.BOOLEAN
		},
    isInvoicedMonthly: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
		accessRequestPassword: {
			allowNull: true,
			type: DataTypes.STRING
		},
		lmsId: {
			allowNull: true,
			type: DataTypes.STRING,
			defaultValue: uuid.v4().toUpperCase()
		},
    moodleTdLmsId: {
      allowNull: true,
      type: DataTypes.INTEGER,
      defaultValue: 0
    },
    moodleTdUsername: {
      allowNull: true,
      type: DataTypes.STRING,
      defaultValue: null
    },
    moodleTdPassword: {
      allowNull: true,
      type: DataTypes.STRING,
      defaultValue: null
    },
    moodleTdaLmsId: {
      allowNull: true,
      type: DataTypes.INTEGER,
      defaultValue: 0
    },
    moodleTdaUsername: {
      allowNull: true,
      type: DataTypes.STRING,
      defaultValue: null
    },
    moodleTdaPassword: {
      allowNull: true,
      type: DataTypes.STRING,
      defaultValue: null
    },
    type: {
      allowNull: true,
      type: DataTypes.STRING
    },
    oldType: {
      allowNull: true,
      type: DataTypes.STRING
    },
    isAllowedImport: {
      allowNull: true,
      type: DataTypes.BOOLEAN
    },
    subscriptionEnabled: {
      allowNull: false,
      type: DataTypes.BOOLEAN
    },
    active: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
      defaultValue: true
    },
    deleted: {
      allowNull: false,
      type: DataTypes.BOOLEAN
    },
    disabled: {
      allowNull: false,
      type: DataTypes.BOOLEAN
    },
    featuresOverridden: {
      allowNull: false,
      type: DataTypes.BOOLEAN
    },
    subscriptionModelOn: {
      allowNull: false,
      type: DataTypes.BOOLEAN
    },
	});
};
