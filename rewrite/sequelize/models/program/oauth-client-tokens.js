const { DataTypes } = require('sequelize');

// We export a function that defines the model.
// This function will automatically receive as parameter the Sequelize connection object.
module.exports = (sequelize) => {
  sequelize.define('oauth_client_tokens', {
    client_id: {
      allowNull: false,
      autoIncrement: false,
      primaryKey: true,
      type: DataTypes.STRING
    },
    token: {
      allowNull: false,
      autoIncrement: false,
      type: DataTypes.STRING
    },
    expiration: {
      allowNull: false,
      type: DataTypes.DATE,
      unique: false
    }
  });
};
