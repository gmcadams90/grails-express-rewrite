const { DataTypes } = require('sequelize');

// We export a function that defines the model.
// This function will automatically receive as parameter the Sequelize connection object.
module.exports = (sequelize) => {
  sequelize.define('import_error', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    errorMessage: {
      allowNull: false,
      type: DataTypes.TEXT,
      unique: false
    },
    lineNumber: {
      allowNull: false,
      type: DataTypes.INTEGER,
      unique: false
    }
  });
};
