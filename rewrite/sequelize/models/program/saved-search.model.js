const { DataTypes } = require('sequelize');

// We export a function that defines the model.
// This function will automatically receive as parameter the Sequelize connection object.
module.exports = (sequelize) => {
  sequelize.define('saved_search', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    route: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: false
    },
    dateCreated: {
      allowNull: false,
      type: DataTypes.DATE,
      unique: false
    },
    lastUpdated: {
      allowNull: false,
      type: DataTypes.DATE,
      unique: false
    },
    name: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: false
    },
    searchData: {
      allowNull: false,
      type: DataTypes.TEXT,
      unique: false
    },
    subscriptionId: {
      allowNull: true,
      type: DataTypes.BIGINT,
      unique: false
    }
  });
};
