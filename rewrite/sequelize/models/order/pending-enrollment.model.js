const { DataTypes } = require('sequelize');

// We export a function that defines the model.
// This function will automatically receive as parameter the Sequelize connection object.
module.exports = (sequelize) => {
  sequelize.define('pending_enrollment', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    dateCreated: {
      allowNull: false,
      type: DataTypes.DATE,
      unique: false
    },
    lastUpdated: {
      allowNull: false,
      type: DataTypes.DATE,
      unique: false
    },
    lmsUserId: {
      allowNull: true,
      type: DataTypes.BIGINT,
      unique: false
    },
    moodleUserIsInstructor: {
      allowNull: false,
      type: DataTypes.BOOLEAN
    },
    oldCostType: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    },
    oldEnrolleeType: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    },
    oldUserFullName: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    },
  });
};
