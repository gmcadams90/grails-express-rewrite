const { DataTypes } = require('sequelize');

// We export a function that defines the model.
// This function will automatically receive as parameter the Sequelize connection object.
module.exports = (sequelize) => {
  sequelize.define('submitted_order', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    alias: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    },
    dateCreated: {
      allowNull: false,
      type: DataTypes.DATE,
      unique: false
    },
    deleted: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
      unique: false
    },
    error: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: false
    },
    oldBilled: {
      allowNull: true,
      type: DataTypes.DATE,
      unique: false
    },
    oldOrderId: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    },
    sequenceNumber: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: true
    },
    status: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: false
    }
  });
};
