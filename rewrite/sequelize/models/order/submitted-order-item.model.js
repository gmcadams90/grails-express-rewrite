const { DataTypes } = require('sequelize');

// We export a function that defines the model.
// This function will automatically receive as parameter the Sequelize connection object.
module.exports = (sequelize) => {
  sequelize.define('submitted_order_item', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    courseName: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: false
    },
    courseNumber: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    },
    instructorPrice: {
      allowNull: false,
      type: DataTypes.FLOAT,
      unique: false,
      min: 0,
      max: 9999.99,
      scale: 2
    },
    newSession: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
      unique: false
    },
    oldCourseCategoryName: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    },
    oldFirstEnrollerUserId: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    },
    oldGroupName: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: true
    },
    oldInstructorEnrollmentTotalPrice: {
      allowNull: true,
      type: DataTypes.FLOAT,
      unique: false
    },
    oldLastEnrollerUserId: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    },
    oldStudentEnrollmentTotalPrice: {
      allowNull: true,
      type: DataTypes.FLOAT,
      unique: false
    },
    sessionName: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    },
    sessionNumber: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    },
    sessionPrice: {
      allowNull: false,
      type: DataTypes.FLOAT,
      unique: false,
      min: 0,
      max: 9999.99,
      scale: 2
    },
    studentPrice: {
      allowNull: false,
      type: DataTypes.FLOAT,
      unique: false,
      min: 0,
      max: 9999.99,
      scale: 2
    }
  });
};
