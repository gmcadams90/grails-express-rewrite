const { DataTypes } = require('sequelize');

// We export a function that defines the model.
// This function will automatically receive as parameter the Sequelize connection object.
module.exports = (sequelize) => {
  sequelize.define('cart_item', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    dateCreated: {
      allowNull: false,
      type: DataTypes.DATE
    },
    lastUpdated: {
      allowNull: false,
      type: DataTypes.DATE
    },
    oldCourseCategoryName: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    },
    oldCourseName: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    },
    oldGroupName: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    },
    oldGroupNumber: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    },
    oldCourseNumber: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    },
    oldStudentEnrollmentUnitPrice: {
      allowNull: true,
      type: DataTypes.FLOAT,
      unique: false
    },
    oldStudentEnrollmentTotalPrice: {
      allowNull: true,
      type: DataTypes.FLOAT,
      unique: false
    },
    oldSessionPrice: {
      allowNull: true,
      type: DataTypes.FLOAT,
      unique: false
    },
    oldInstructorEnrollmentUnitPrice: {
      allowNull: true,
      type: DataTypes.FLOAT,
      unique: false
    },
    oldInstructorEnrollmentTotalPrice: {
      allowNull: true,
      type: DataTypes.FLOAT,
      unique: false
    },
    oldFirstEnrollerUserId: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    },
    oldLastEnrollerUserId: {
      allowNull: true,
      type: DataTypes.FLOAT,
      unique: false
    },
    deleted: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
      unique: false
    }
  });
};
