const { DataTypes } = require('sequelize');

// We export a function that defines the model.
// This function will automatically receive as parameter the Sequelize connection object.
module.exports = (sequelize) => {
  sequelize.define('submitted_enrollment', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    dateCreated: {
      allowNull: false,
      type: DataTypes.DATE,
      unique: false
    },
    email: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    },
    firstName: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    },
    isInstructor: {
      allowNull: false,
      type: DataTypes.BOOLEAN
    },
    isTransfer: {
      allowNull: false,
      type: DataTypes.BOOLEAN
    },
    lastName: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    },
    lmsUserId: {
      allowNull: true,
      type: DataTypes.BIGINT,
      unique: false
    },
    oldCostType: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    },
    oldEnrolleeType: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    },
    oldFullName: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    },
    studentTypeName: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: false
    }
  });
};
