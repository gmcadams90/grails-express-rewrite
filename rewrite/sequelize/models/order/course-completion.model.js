const { DataTypes } = require('sequelize');

// We export a function that defines the model.
// This function will automatically receive as parameter the Sequelize connection object.
module.exports = (sequelize) => {
  sequelize.define('course_completion', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    courseName: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: false
    },
    dateCreated: {
      allowNull: false,
      type: DataTypes.DATE
    },
    lastUpdated: {
      allowNull: false,
      type: DataTypes.DATE
    },
    sessionName: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    },
    studentName: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: false
    },
    testScore: {
      allowNull: true,
      type: DataTypes.FLOAT,
      unique: false
    },
    dateCompleted: {
      allowNull: true,
      type: DataTypes.DATE,
      unique: false
    },
    status: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    },
    practical: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    },
    finalGrade: {
      allowNull: true,
      type: DataTypes.FLOAT,
      unique: false
    },
    subscriptionId: {
      allowNull: true,
      type: DataTypes.BIGINT,
      unique: false
    }
  });
};
