const { DataTypes } = require('sequelize');

// We export a function that defines the model.
// This function will automatically receive as parameter the Sequelize connection object.
module.exports = (sequelize) => {
  sequelize.define('course_group', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    clcsWorksheet: {
      allowNull: false,
      type: DataTypes.FLOAT,
      unique: false
    },
    clcsWorksheetYear: {
      allowNull: false,
      type: DataTypes.FLOAT,
      unique: false
    },
    dateCreated: {
      allowNull: false,
      type: DataTypes.DATE,
      unique: false
    },
    lastUpdated: {
      allowNull: false,
      type: DataTypes.DATE,
      unique: false
    },
    name: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
      unique: false
    },
    oldClcsImportedId: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
      unique: false
    },
    oldCourseGroupId: {
      allowNull: true,
      type: DataTypes.FLOAT,
      unique: false
    },
    deleted: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
      unique: false
    }
  });
};
