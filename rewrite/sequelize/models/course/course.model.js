const { DataTypes } = require('sequelize');

// We export a function that defines the model.
// This function will automatically receive as parameter the Sequelize connection object.
module.exports = (sequelize) => {
  sequelize.define('course', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    deleted: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
      unique: false
    },
    instructorPrice: {
      allowNull: false,
      type: DataTypes.FLOAT,
      unique: false
    },
    sessionPrice: {
      allowNull: false,
      type: DataTypes.FLOAT,
      unique: false
    },
    studentPrice: {
      allowNull: false,
      type: DataTypes.FLOAT,
      unique: false
    },
    dateCreated: {
      allowNull: false,
      type: DataTypes.DATE,
      unique: false
    },
    lastUpdated: {
      allowNull: false,
      type: DataTypes.DATE,
      unique: false
    },
    isInstructorOnly: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
      unique: false
    },
    isInstructorRequired: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
      unique: false
    },
    oldPreReqLmsCourseId: {
      allowNull: true,
      type: DataTypes.FLOAT,
      unique: false
    },
    oldPreReqLmsCourseName: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    },
    excludeGradesManage: {
      allowNull: true,
      type: DataTypes.BOOLEAN,
      unique: false,
      defaultValue: false
    },
    infiniteLicenses: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
      unique: false
    },
    autoEnrollmentEnabled: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
      unique: false
    }
  });
};
