const { DataTypes } = require('sequelize');

// We export a function that defines the model.
// This function will automatically receive as parameter the Sequelize connection object.
module.exports = (sequelize) => {
  sequelize.define('custom_course', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    dateCreated: {
      allowNull: true,
      type: DataTypes.DATE
    },
    lastUpdated: {
      allowNull: true,
      type: DataTypes.DATE
    },
    courseName: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: false
    },
    shortName: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: false
    },
    summary: {
      allowNull: true,
      type: DataTypes.TEXT,
      unique: false
    },
    lmsId: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: true
    },
    numberOfLessons: {
      allowNull: true,
      type: DataTypes.BIGINT,
      unique: true
    },
    deleted: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
      unique: false
    },
    status: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: false
    }
  });
};
