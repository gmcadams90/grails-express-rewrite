const { DataTypes } = require('sequelize');

// We export a function that defines the model.
// This function will automatically receive as parameter the Sequelize connection object.
module.exports = (sequelize) => {
  sequelize.define('course_session', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    courseGroupSession: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
      unique: false
    },
    dateCreated: {
      allowNull: true,
      type: DataTypes.DATE
    },
    deleted: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
      unique: false
    },
    isChildGroupSession: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
      unique: false
    },
    lastUpdated: {
      allowNull: true,
      type: DataTypes.DATE
    },
    lmsId: {
      allowNull: true,
      type: DataTypes.BIGINT,
      unique: true
    },
    name: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    },
    oldCourseCategoryName: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    },
    oldCourseGroupSessionId: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    },
    oldCourseName: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    },
    oldCourseNumber: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    },
    oldFirstEnrollerUserId: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    },
    oldGroupNumber: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    },
    oldInstructorEnrollmentTotalPrice: {
      allowNull: true,
      type: DataTypes.FLOAT,
      unique: false
    },
    oldInstructorEnrollmentUnitPrice: {
      allowNull: true,
      type: DataTypes.FLOAT,
      unique: false
    },
    oldIsNew: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    },
    oldLastEnrollerUserId: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    },
    oldSessionPrice: {
      allowNull: true,
      type: DataTypes.FLOAT,
      unique: false
    },
    oldStudentEnrollmentTotalPrice: {
      allowNull: true,
      type: DataTypes.FLOAT,
      unique: false
    },
    oldStudentEnrollmentUnitPrice: {
      allowNull: true,
      type: DataTypes.FLOAT,
      unique: false
    },
    pending: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
      unique: false
    },
    transitory: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
      unique: false
    },
    startDate: {
      allowNull: true,
      type: DataTypes.DATE
    }
  });
};
