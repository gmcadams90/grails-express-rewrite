const { DataTypes } = require('sequelize');

// We export a function that defines the model.
// This function will automatically receive as parameter the Sequelize connection object.
module.exports = (sequelize) => {
  sequelize.define('access_log', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    ipAddress: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: false,
      validate: {
        max: 255
      }
    },
    timestamp: {
      allowNull: false,
      type: DataTypes.DATE,
      unique: false
    },
  });
};
