const { DataTypes } = require('sequelize');

// We export a function that defines the model.
// This function will automatically receive as parameter the Sequelize connection object.
module.exports = (sequelize) => {
  sequelize.define('access_window', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    days: {
      allowNull: false,
      type: DataTypes.INTEGER,
      unique: false
    },
    dateCreated: {
      allowNull: false,
      type: DataTypes.DATE
    },
    price: {
      allowNull: true,
      type: DataTypes.INTEGER,
      unique: false
    },
    name: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: false
    },
    status: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: false,
      defaultValue: "open"
    },
    description: {
      allowNull: true,
      type: DataTypes.TEXT,
      unique: false
    },
    oneTimeOnly: {
      allowNull: true,
      type: DataTypes.BOOLEAN,
      defaultValue: false
    }
  });
};
