const { DataTypes } = require('sequelize');

// We export a function that defines the model.
// This function will automatically receive as parameter the Sequelize connection object.
module.exports = (sequelize) => {
  sequelize.define('access_token', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    dateCreated: {
      allowNull: false,
      type: DataTypes.DATE,
      unique: false
    },
    price: {
      allowNull: false,
      type: DataTypes.INTEGER,
      unique: false
    },
    days: {
      allowNull: false,
      type: DataTypes.INTEGER,
      unique: false
    },
    dateStarted: {
      allowNull: true,
      type: DataTypes.DATE,
      unique: false
    },
    canceledAt: {
      allowNull: true,
      type: DataTypes.DATE,
      unique: false
    },
    quickbooksCancelId: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    },
    quickbooksCreationId: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    }
  });
};
