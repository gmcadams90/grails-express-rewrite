const { DataTypes } = require('sequelize');

// We export a function that defines the model.
// This function will automatically receive as parameter the Sequelize connection object.
module.exports = (sequelize) => {
  sequelize.define('enrollment_transaction_course', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    courseName: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    },
    dateCreated: {
      allowNull: false,
      type: DataTypes.DATE,
      unique: false
    },
    groupId: {
      allowNull: true,
      type: DataTypes.BIGINT,
      unique: false
    },
    lineItemId: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: false
    },
    sessionIsNew: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
      unique: false
    },
    groupName: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    }
  });
};
