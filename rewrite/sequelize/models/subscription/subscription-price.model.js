const { DataTypes } = require('sequelize');

// We export a function that defines the model.
// This function will automatically receive as parameter the Sequelize connection object.
module.exports = (sequelize) => {
  sequelize.define('subscription_price', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    dateCreated: {
      allowNull: false,
      type: DataTypes.DATE
    },
    billing: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    },
    lastUpdated: {
      allowNull: false,
      type: DataTypes.DATE
    },
    price: {
      allowNull: true,
      type: DataTypes.INTEGER,
      unique: false
    },
    term: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: false,
      defaultValue: "yearly"
    },
  });
};
