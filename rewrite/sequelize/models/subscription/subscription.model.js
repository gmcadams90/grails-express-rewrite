const { DataTypes } = require('sequelize');

// We export a function that defines the model.
// This function will automatically receive as parameter the Sequelize connection object.
module.exports = (sequelize) => {
  sequelize.define('subscription', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    dateCreated: {
      allowNull: false,
      type: DataTypes.DATE,
      unique: false
    },
    canceledAt: {
      allowNull: true,
      type: DataTypes.DATE,
      unique: false
    },
    commitmentStartDate: {
      allowNull: false,
      type: DataTypes.DATE,
      unique: false
    },
    quickbooksCreationId: {
      allowNull: false,
      type: DataTypes.DATE,
      unique: false
    },
    nextRenewalDate: {
      allowNull: false,
      type: DataTypes.DATE,
      unique: false
    },
    quickbooksCancelId: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    },
    canceledPrice: {
      allowNull: true,
      type: DataTypes.INTEGER,
      unique: false
    },
    proratedPrice: {
      allowNull: true,
      type: DataTypes.INTEGER,
      unique: false
    },
    lastRenewalDate: {
      allowNull: true,
      type: DataTypes.DATE
    }
  });
};
