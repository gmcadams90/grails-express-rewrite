const { DataTypes } = require('sequelize');

// We export a function that defines the model.
// This function will automatically receive as parameter the Sequelize connection object.
module.exports = (sequelize) => {
  sequelize.define('access_transaction', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    dateCreated: {
      allowNull: false,
      type: DataTypes.DATE
    },
    totalPrice: {
      allowNull: true,
      type: DataTypes.INTEGER,
      unique: false
    },
    action: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: false
    }
  });
};
