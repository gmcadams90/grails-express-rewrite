const { DataTypes } = require('sequelize');

// We export a function that defines the model.
// This function will automatically receive as parameter the Sequelize connection object.
module.exports = (sequelize) => {
  sequelize.define('assigned_license', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    courseName: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    },
    itemCode: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: false
    },
    dateCreated: {
      allowNull: false,
      type: DataTypes.DATE
    },
    dateExpired: {
      allowNull: true,
      type: DataTypes.DATE
    },
    license: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: false
    },
    viewed: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
      unique: false
    },
    status: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: false
    }
  });
};
