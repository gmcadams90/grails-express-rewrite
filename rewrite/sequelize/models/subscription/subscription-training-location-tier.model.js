const { DataTypes } = require('sequelize');

// We export a function that defines the model.
// This function will automatically receive as parameter the Sequelize connection object.
module.exports = (sequelize) => {
  sequelize.define('subscription_training_location_tier', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    name: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: true
    },
    monthlyCost: {
      allowNull: false,
      type: DataTypes.FLOAT,
      unique: false
    },
    quarterlyCost: {
      allowNull: false,
      type: DataTypes.FLOAT,
      unique: false
    },
    yearlyCost: {
      allowNull: false,
      type: DataTypes.FLOAT,
      unique: false
    }
  });
};
