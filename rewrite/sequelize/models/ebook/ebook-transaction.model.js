const { DataTypes } = require('sequelize');

// We export a function that defines the model.
// This function will automatically receive as parameter the Sequelize connection object.
module.exports = (sequelize) => {
  sequelize.define('ebook_transaction', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    item_code: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: false
    },
    dateCreated: {
      allowNull: false,
      type: DataTypes.DATE
    },
    lastUpdated: {
      allowNull: false,
      type: DataTypes.DATE
    },
    license: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: false
    },
    status: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: false
    },
    bookName: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    },
    firstName: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    },
    lastName: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    }
  });
};
