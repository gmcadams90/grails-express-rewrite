const { DataTypes } = require('sequelize');
const uuid = require('uuid');

// We export a function that defines the model.
// This function will automatically receive as parameter the Sequelize connection object.
module.exports = (sequelize) => {
  sequelize.define('readynamic_create_account_access_token', {
    id: {
      allowNull: false,
      autoIncrement: false,
      primaryKey: true,
      type: DataTypes.STRING,
      defaultValue: uuid.v4()
    },
    version: {
      allowNull: false,
      type: DataTypes.BIGINT,
      unique: false
    },
    dateCreated: {
      allowNull: false,
      type: DataTypes.DATE,
      unique: false
    },
    expiryDate: {
      allowNull: false,
      type: DataTypes.DATE,
      unique: false
    },
    lmsUserEmail: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: false
    },
    passwordResetLink: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    },
    subscriptionId: {
      allowNull: true,
      type: DataTypes.BIGINT,
      unique: false
    }
  });
};
