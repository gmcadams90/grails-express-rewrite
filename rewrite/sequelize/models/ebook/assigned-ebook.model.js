const { DataTypes } = require('sequelize');

// We export a function that defines the model.
// This function will automatically receive as parameter the Sequelize connection object.
module.exports = (sequelize) => {
  sequelize.define('assigned_ebook', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    license: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: false
    },
    item_code: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: false
    },
    status: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: false,
      defaultValue: "active"
    },
    readynamic_book_id: {
      allowNull: false,
      type: DataTypes.BIGINT,
      unique: false
    },
    readynamic_annotation_set_id: {
      allowNull: false,
      type: DataTypes.BIGINT,
      unique: false
    },
    readynamic_acl_contact_id: {
      allowNull: false,
      type: DataTypes.BIGINT,
      unique: false
    },
    viewed: {
      allowNull: false,
      type: DataTypes.BOOLEAN
    }
  });
};
