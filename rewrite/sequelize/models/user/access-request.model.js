const { DataTypes } = require('sequelize');

// We export a function that defines the model.
// This function will automatically receive as parameter the Sequelize connection object.
module.exports = (sequelize) => {
  sequelize.define('access_request', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    approved: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    },
    dateCreated: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: false
    },
    deleted: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
      unique: false
    },
    email: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: false
    },
    firstName: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: false
    },
    isInstructor: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
      unique: false
    },
    lastName: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: false
    },
    lastUpdated: {
      allowNull: false,
      type: DataTypes.DATE,
      unique: false
    },
    lmsUserId: {
      allowNull: true,
      type: DataTypes.BIGINT,
      unique: false
    },
    oldAccessRequestId: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    },
    oldCourses: {
      allowNull: true,
      type: DataTypes.TEXT,
      unique: false
    },
    password: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    },
    phone: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    },
    status: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: false
    },
    subscriptionId: {
      allowNull: true,
      type: DataTypes.BIGINT,
      unique: false
    }
  });
};
