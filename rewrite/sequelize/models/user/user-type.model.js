const { DataTypes } = require('sequelize');

// We export a function that defines the model.
// This function will automatically receive as parameter the Sequelize connection object.
module.exports = (sequelize) => {
  sequelize.define('user_type', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    version: {
      allowNull: false,
      type: DataTypes.TEXT,
      unique: false
    },
    title: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: false
    },
    defaultStudentType: {
      allowNull: false,
      type: DataTypes.BOOLEAN
    },
    defaultInstructorType: {
      allowNull: false,
      type: DataTypes.BOOLEAN
    },
    abbr: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: false
    },
    programClassificationEligible: {
      allowNull: false,
      type: DataTypes.BOOLEAN
    },
    colorCode: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: false
    },
    hasLmsAccount: {
      allowNull: false,
      type: DataTypes.BOOLEAN
    },
    category: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: false
    }
  });
};
