const { DataTypes } = require('sequelize');

// We export a function that defines the model.
// This function will automatically receive as parameter the Sequelize connection object.
module.exports = (sequelize) => {
	sequelize.define('permission', {
		id: {
			allowNull: false,
			autoIncrement: true,
			primaryKey: true,
			type: DataTypes.INTEGER
		},
		slug: {
			allowNull: false,
			type: DataTypes.STRING,
			unique: false
		},
		displayName: {
			allowNull: false,
			type: DataTypes.STRING,
			unique: false
		}
	});
};
