const { DataTypes } = require('sequelize');
const { isValidEmail } = require('../../../express/helpers');

// We export a function that defines the model.
// This function will automatically receive as parameter the Sequelize connection object.
module.exports = (sequelize) => {
	sequelize.define('ea_user', {
		id: {
			allowNull: false,
			autoIncrement: true,
			primaryKey: true,
			type: DataTypes.INTEGER
		},
		dateCreated: {
			allowNull: false,
			type: DataTypes.DATE
		},
    deleted: {
      allowNull: false,
      type: DataTypes.BOOLEAN
    },
		lastUpdated: {
			allowNull: false,
			type: DataTypes.DATE
		},
		firstName: {
			allowNull: false,
			type: DataTypes.STRING,
			validate: {
				max: 255
			}
		},
		lastName: {
			allowNull: false,
			type: DataTypes.STRING,
			validate: {
				max: 255
			}
		},
		phone: {
			allowNull: true,
			type: DataTypes.STRING
		},
		email: {
			allowNull: true,
			type: DataTypes.STRING,
			validate: {
				verifyEmail(value) {
					if (value === null && isValidEmail(value) === false) {
						throw new Error("Email is invalid");
					}
				}
			}
		},
		active: {
			allowNull: false,
			type: DataTypes.BOOLEAN
		},
		password: {
			allowNull: true,
			type: DataTypes.STRING
		}
	});
};
