const { DataTypes } = require('sequelize');

// We export a function that defines the model.
// This function will automatically receive as parameter the Sequelize connection object.
module.exports = (sequelize) => {
	sequelize.define('permission_role', {
		id: {
			allowNull: false,
			autoIncrement: true,
			primaryKey: true,
			type: DataTypes.INTEGER
		},
		name: {
			allowNull: false,
			type: DataTypes.STRING,
			unique: false
		},
		deleted: {
			allowNull: false,
			type: DataTypes.BOOLEAN,
			unique: false,
			defaultValue: false
		},
		isDefault: {
			allowNull: true,
			type: DataTypes.BOOLEAN,
			unique: false,
			defaultValue: false
		},
		createByAdminOnly: {
			allowNull: false,
			type: DataTypes.BOOLEAN,
			unique: false,
			defaultValue: false
		}
	});
};
