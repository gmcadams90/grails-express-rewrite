const { DataTypes } = require('sequelize');
const uuid = require('uuid');

// We export a function that defines the model.
// This function will automatically receive as parameter the Sequelize connection object.
module.exports = (sequelize) => {
  sequelize.define('program_classification', {
    id: {
      allowNull: false,
      autoIncrement: false,
      primaryKey: true,
      type: DataTypes.STRING,
      defaultValue: uuid.v4()
    },
    version: {
      allowNull: false,
      type: DataTypes.BIGINT,
      unique: false
    },
    abbr: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: false,
      validate: {
        max: 10
      }
    },
    name: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: false
    }
  });
};
