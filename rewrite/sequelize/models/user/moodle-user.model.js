const { DataTypes } = require('sequelize');

// We export a function that defines the model.
// This function will automatically receive as parameter the Sequelize connection object.
module.exports = (sequelize) => {
  sequelize.define('moodle_user', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    deleted: {
      allowNull: false,
      type: DataTypes.BOOLEAN
    },
    readynamicUserId: {
      allowNull: true,
      type: DataTypes.BIGINT,
      unique: false
    },
    activationDate: {
      allowNull: true,
      type: DataTypes.DATE,
      unique: false
    },
    active: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    },
    address1: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    },
    address2: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    },
    altPhone: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    },
    autoRenew: {
      allowNull: true,
      type: DataTypes.BOOLEAN,
      unique: false
    },
    city: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    },
    country: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    },
    dateCreated: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    },
    email: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    },
    endDate: {
      allowNull: true,
      type: DataTypes.DATE,
      unique: false
    },
    firstName: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    },
    isInstructor: {
      allowNull: true,
      type: DataTypes.BOOLEAN,
      unique: false
    },
    lastName: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    },
    lmsUserId: {
      allowNull: true,
      type: DataTypes.BIGINT,
      unique: false
    },
    memberNumber: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    },
    pendingDate: {
      allowNull: true,
      type: DataTypes.DATE,
      unique: false
    },
    state: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    },
    zip: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    },
    subscriptionType: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    },
    middleInitial: {
      allowNull: true,
      type: DataTypes.CHAR,
      unique: false
    },
    suffix: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    },
    removed: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
      unique: false,
      defaultValue: false
    },
    cellPhone: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    }
  });
};
