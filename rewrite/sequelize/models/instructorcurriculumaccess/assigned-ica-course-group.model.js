const { DataTypes } = require('sequelize');

// We export a function that defines the model.
// This function will automatically receive as parameter the Sequelize connection object.
module.exports = (sequelize) => {
  sequelize.define('assigned_ica_course_group', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    dateCreated: {
      allowNull: false,
      type: DataTypes.DATE
    },
    dateExpires: {
      allowNull: false,
      type: DataTypes.DATE
    },
    license: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: false
    }
  });
};
