const { DataTypes } = require('sequelize');

// We export a function that defines the model.
// This function will automatically receive as parameter the Sequelize connection object.
module.exports = (sequelize) => {
  sequelize.define('ica_license', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    dateCreated: {
      allowNull: false,
      type: DataTypes.DATE
    },
    license: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: false
    },
    dateEnded: {
      allowNull: false,
      type: DataTypes.DATE
    }
  });
};
