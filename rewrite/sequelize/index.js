const { Sequelize } = require('sequelize');
const { applyExtraSetup } = require('./extra-setup');
const config = require('../express/config')

const host = config.dataSource.host
const port = config.dataSource.port
const database = config.dataSource.database
const username = config.dataSource.username
const password = config.dataSource.password

// In a real app, you should keep the database connection URL as an environment variable.
// But for this example, we will just use a local SQLite database.
// const sequelize = new Sequelize(process.env.DB_CONNECTION_URL);
//database wide options
const sequelize = new Sequelize(database, username, password, {
	host: host,
	port: port,
	dialect: 'mysql',
	define: {
		timestamps: false,
		freezeTableName: true,
		underscored: true,
		version: true
	}
});
const modelDefiners = [
	require('./models/user/permission.model'),
	require('./models/user/permission-role.model'),
	require('./models/program/training-location.model'),
	require('./models/user/ea-user.model'),
  require('./models/subscription/subscription-training-location-tier.model'),
  require('./models/order/cart-item.model'),
  require('./models/course/course.model'),
  require('./models/course/course-group.model'),
  require('./models/course/course-session.model'),
  require('./models/course/custom-course.model'),
  require('./models/course/prerequisite.model'),
  require('./models/ebook/assigned-ebook.model'),
  require('./models/ebook/ebook-transaction.model'),
  require('./models/ebook/ebook-transaction-group.model'),
  require('./models/user/user-type.model'),
  require('./models/program/import-error.model'),
  require('./models/ebook/readynamic-create-account-access-token.model'),
  require('./models/order/completion-transaction.model'),
  require('./models/access-log.model'),
  require('./models/user/program-classification.model'),
  require('./models/order/pending-enrollment.model'),
  require('./models/order/submitted-enrollment.model'),
  require('./models/order/submitted-order.model'),
  require('./models/order/submitted-order-item.model'),
  require('./models/qualification/qualification-type.model'),
  require('./models/subscription/enrollment-transaction.model'),
  require('./models/user/moodle-user.model'),
  require('./models/subscription/subscription-feature.model'),
  require('./models/subscription/enrollment-transaction-course-user.model'),
  require('./models/subscription/enrollment-transaction-course.model'),
  require('./models/program/training-location-group.model'),
  require('./models/user/access-request.model'),
  require('./models/user/filter-group.model'),
  require('./models/subscription/subscription.model'),
  require('./models/subscription/subscription-price.model'),
  require('./models/subscription/subscription-user.model'),
  require('./models/order/course-completion.model'),
  require('./models/program/saved-search.model'),
  require('./models/program/import-run.model'),
  require('./models/user/student-type.model'),
  require('./models/program/configuration.model'),
  require('./models/accesstoken/access-token.model'),
  require('./models/accesstoken/access-token-user.model'),
  require('./models/accesstoken/access-window.model'),
  require('./models/accesstoken/training-location-access-window.model'),
  require('./models/instructorcurriculumaccess/assigned-ica-course-group.model'),
  require('./models/instructorcurriculumaccess/assigned-ica-license.model'),
  require('./models/instructorcurriculumaccess/ica-course-group.model'),
  require('./models/instructorcurriculumaccess/ica-license.model'),
  require('./models/subscription/assigned-license.model'),
  require('./models/subscription/access-transaction.model'),
  require('./models/program/oauth-user-tokens'),
  require('./models/program/oauth-client-details'),
  require('./models/program/oauth-client-tokens')
];

// We define all models according to their files.
for (const modelDefiner of modelDefiners) {
	modelDefiner(sequelize);
}

// We execute any extra setup after the models are defined, such as adding associations.
applyExtraSetup(sequelize);

// We export the sequelize connection instance to be used around our app.
module.exports = sequelize;
