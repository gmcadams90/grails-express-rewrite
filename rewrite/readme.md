## Getting Started

* Install dependencies with `npm install` or `yarn install`
* Run the express server with `npm start`
* Open your browser in `localhost:8000` and try the example REST endpoints:
	* `localhost:8000/api/permissions` (GET)
	* `localhost:8000/api/permissions/1` (GET)
	* `localhost:8000/api/permissions` (POST)
		* Body format: `Expects JSON API spec from Grails`
	* `localhost:8000/api/permissions/1` (DELETE)
	* `etc`

## License

MIT
